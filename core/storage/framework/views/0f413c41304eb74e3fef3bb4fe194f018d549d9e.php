
<?php $__env->startSection('css'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<section id="header" class="backg backg-one bg-banner-gradient">
    <div class="container">
        <div class="backg-content-wrap">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="backg-content">
                        <span class="discount wow soneFadeUp" data-wosw-delay="0.3s"><?php echo e($set->title); ?></span>
                        <h1 class="backg-title wow soneFadeUp" data-wow-delay="0.5s">
                        <?php echo e($ui->header_title); ?>

                        </h1>
                        <p class="description wow soneFadeUp text-dark" data-wow-delay="0.6s">
                        <?php echo e($ui->header_body); ?>

                        </p>
                        <a href="<?php echo e(route('register')); ?>" class="pxs-btn backg-btn wow soneFadeUp" data-wow-delay="0.6s"><?php echo e(__('Get Started')); ?></a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="promo-mockup wow soneFadeLeft">
                        <img src="<?php echo e(url('/')); ?>/asset/images/<?php echo e($ui->s6_image); ?>" alt="header">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-shape bg-shape-bottom">
        <img src="<?php echo e(url('/')); ?>/asset/images/shape-bg.png">
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="feature-box">
                    <div class="row">
                        <div class="col-lg-6 flex-center">
                            <div class="featured-icon-box-wrapper style-five color-1">
                                <div class="featured-icon-box-icon"><img src="<?php echo e(url('/')); ?>/asset/images/<?php echo e($ui->s2_image); ?>"></div>
                                <div class="featured-icon-box-content">
                                    <span class="featured-icon-box-title"><?php echo e($ui->item1_title); ?></span>
                                    <p><?php echo e($ui->item1_body); ?></p>
                                </div>
                            </div>                            
                        </div>
                        <div class="col-lg-6 ">
                            <div class="featured-icon-box-wrapper style-five color-1">
                                <div class="featured-icon-box-icon"><img src="<?php echo e(url('/')); ?>/asset/images/<?php echo e($ui->s3_image); ?>"></div>
                                <div class="featured-icon-box-content">
                                    <span class="featured-icon-box-title"><?php echo e($ui->item2_title); ?></span>
                                    <p><?php echo e($ui->item2_body); ?></p>
                                </div>
                            </div>
                            <div class="featured-icon-box-wrapper style-five color-1">
                                <div class="featured-icon-box-icon"><img src="<?php echo e(url('/')); ?>/asset/images/<?php echo e($ui->s4_image); ?>"></div>
                                <div class="featured-icon-box-content">
                                    <span class="featured-icon-box-title"><?php echo e($ui->item3_title); ?></span>
                                    <p><?php echo e($ui->item3_body); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 flex-center">
                <div class="section-title style-two">
                    <h2 class="title"><?php echo e($ui->s2_title); ?></h2>
                    <p><?php echo e($ui->s2_body); ?></p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="bg-process">
    <div class="bg-overlay-testi"></div>
    <div class="container">
        <div class="row">
        <div class="col-lg-6">
            <div class="editure-feature-image">
                <div class="image-one">
                    <img src="<?php echo e(url('/')); ?>/asset/images/<?php echo e($ui->s7_image); ?>" class="wow soneFadeRight r10" data-wow-delay="0.3s" alt="feature-image">
                </div>
            </div>
        </div>
            <div class="col-lg-6">
                <div class="img-text-content">
                    <div class="section-title">
                        <h2 class="title"><?php echo e($ui->s6_title); ?></h2>
                        <p><?php echo e($ui->s6_body); ?></p>
                    </div>
                    <a href="<?php echo e(route('about')); ?>" class="sone-btn btn-light"><?php echo e(__('Learn More')); ?></a>
                </div>
                <!-- /.img-text-content -->
            </div>
            <!-- /.col-lg-6 -->
        </div>
        <!-- /.row -->
    </div>
</section>
<section class="services pt-150 wow soneFadeUp">
    <div class="container">
        <div class="section-title text-center">
            <span class="sub-title"><?php echo e(__('Services')); ?></span>
            <h2 class="title"><?php echo e(__('The Most Trusted Trading Platform')); ?></h2>
            <p><?php echo e(__('Here are a few reasons why you should choose us')); ?></p>
        </div>

        <div class="row gap-y">
        <?php $__currentLoopData = $service; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $services): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="col-md-6 col-xl-3">
                <div class="services-box-wrapper text-center">
                    <div class="my-3 services-box-icon color-1"><i class="fa fa-<?php echo e($services->icon); ?>"></i></div>
                    <span class="mb-20 fw-500 text-dark"><?php echo e($services->title); ?></span>
                    <p class="castrooo"><?php echo e($services->details); ?></p>
                </div>
            </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
</section>
<section class="pricing-two pt-100 wow soneFadeUp">
    <div class="container">
        <div class="section-title text-center">
            <span class="sub-title"><?php echo e(__('Pricing Plan')); ?></span>
            <h2 class="title">
            <?php echo e(__('Choose your pricing policy which affordable')); ?>

            </h2>
        </div>
        <div class="row advanced-pricing-table no-gutters">
            <?php $__currentLoopData = $plan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="col-lg-3">
                <div class="pricing-table">
                    <div class="pricing-header pricing-amount">
                        <h3 class="price-title"><?php echo e($val->name); ?></h3>
                        <p><?php echo e(__('Payouts wont be availabe till end of plan duration')); ?></p>
                        <div class="annual_price">
                            <h2 class="price"><?php echo e($currency->symbol.$val->min_deposit); ?></h2>
                        </div>
                        <div class="monthly_price">
                            <h2 class="price"><?php echo e($currency->symbol.number_format($val->min_deposit)); ?></h2>
                        </div>
                        <div class="small_desc text-center">
                            <a href="javascript:void"><?php echo e(__('Profit Topup is Automated')); ?></a><br>
                            <a href="javascript:void"><?php echo e(__('For')); ?> <?php echo e($val->duration); ?> <?php echo e($val->period); ?>(s)</a><br>
                            <a href="javascript:void"><?php echo e($val->percent); ?> <?php echo e(__('Daily Percent')); ?></a><br>
                            <a href="javascript:void"><?php echo e($currency->symbol.number_format($val->amount)); ?> <?php echo e(__('Maximum Price')); ?></a><br>
                            <?php if($val->ref_percent!=null): ?>
                            <a href="javascript:void"><?php echo e($val->ref_percent); ?>% <?php echo e(__('Referral Percent')); ?></a><br>
                            <?php endif; ?>
                            <?php if($val->bonus!=null): ?>
                            <a href="javascript:void"><?php echo e($val->bonus); ?>% <?php echo e(__('Trading Bonus')); ?></a><br>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
</section>
<?php if(count($team)>0): ?>
<section class="teams-single wow soneFadeUp">
    <div class="container">
        <div class="section-title text-center">
            <h3 class="sub-title"><?php echo e(__('Our Team')); ?></h3>
            <h2 class="title"><?php echo e(__('The Experts Team')); ?></h2>
            <p> <?php echo e($ui->team); ?></p>
        </div>
        <div class="row">
        <?php $__currentLoopData = $team; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="col-sm-4">
                <div class="team-member">
                    <div class="member-avater"><img src="<?php echo e(url('/')); ?>/asset/review/<?php echo e($val->image); ?>" alt="avater">
                        <div class="layer-one">
                            <div class="team-info">
                                <span class="name"><?php echo e($val->name); ?></span>
                                <p class="job"><?php echo e($val->position); ?></p>
                            </div>
                        </div>

                        <ul class="member-social">
                            <li><a href="<?php echo e($val->facebook); ?>"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="<?php echo e($val->twitter); ?>"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="<?php echo e($val->linkedin); ?>"><i class="fab fa-linkedin-in"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
    <div class="shape-bg-left-circle">
        <div class="shape-circle-animated"></div>            
    </div>
</section>
<?php endif; ?>
<section class="countup bg-testi">
    <div class="container">
        <div class="section-title text-center">
            <span class="sub-title"><?php echo e(__('Get Assured Profits')); ?></span>
            <h2 class="title"><?php echo e($ui->s8_title); ?></h2>
            <p><?php echo e($ui->s8_body); ?></p>
        </div>
        <div class="countup-wrapper">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="fun-fact text-center">
                        <div class="counter">
                            <h4 class="count"><?php echo e($ui->total_assets); ?></h4></div>
                        <p><?php echo e($ui->x1); ?></p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="fun-fact text-center">
                        <div class="counter">
                            <h4 class="count"><?php echo e($ui->experience); ?></h4></div>
                        <p><?php echo e($ui->x2); ?></p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="fun-fact text-center">
                        <div class="counter">
                            <h4 class="count"><?php echo e($ui->traders); ?></h4></div>
                        <p><?php echo e($ui->x3); ?></p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="fun-fact text-center">
                        <div class="counter">
                            <h4 class="count"><?php echo e($ui->countries); ?></h4></div>
                        <p><?php echo e($ui->x4); ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php if(count($review)>0): ?>
<section class="testimonials-two wow soneFadeUp" id="testimonialxx">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
            <div class="section-title">
                <span class="sub-title"><?php echo e(__('Reviews')); ?></span>
                <h2 class="title"><?php echo e($ui->s7_title); ?></h2>
                <p><?php echo e($ui->s7_body); ?></p>
            </div>
        </div>
        <div class="col-lg-8">
            <div id="testimonial-wrapper">
                <div class="swiper-container" id="testimonial-two" data-speed="700" data-autoplay="5000" data-perpage="2" data-space="50" data-breakpoints='{"991": {"slidesPerView": 1}}'>
                    <div class="swiper-wrapper">
                        <?php $__currentLoopData = $review; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vreview): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="swiper-slide">
                                <div class="testimonial-two">
                                    <div class="testi-content-inner">
                                        <div class="testimonial-bio">
                                            <div class="avatar"><img src="<?php echo e(url('/')); ?>/asset/review/<?php echo e($vreview->image_link); ?>" alt="testimonial"></div>
                                            <div class="bio-info">
                                                <h4 class="name"><?php echo e($vreview->name); ?></h4>
                                                <span class="job"><?php echo e($vreview->occupation); ?></span></div>
                                        </div>
                                        <div class="testimonial-content">
                                            <p><?php echo e($vreview->review); ?></p>
                                        </div>   
                                        <ul class="rating">
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                        </ul>     
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
</section>
<?php endif; ?>
<section class="services address-contact">
    <div class="container">
        <div class="section-title text-center">
            <h2 class="title"><?php echo e(__('Don’t hesitate to contact us for')); ?><br><?php echo e(__('any information.')); ?></h2>
        </div>
        <div class="row gap-y">
            <div class="col-md-4">
                <div class="services-box-wrapper text-center">
                    <div class="my-3 services-box-icon color-1"><i class="fas fa-map-marker-alt xcolls"></i></div>
                    <span class="mb-20 fw-500 text-dark"><?php echo e(__('Our Location')); ?></span>
                    <p class="castrooo"><?php echo e($set->address); ?></p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="services-box-wrapper text-center">
                    <div class="my-3 services-box-icon color-1"><i class="fas fa-border-all xcolls"></i></div>
                    <span class="mb-20 fw-500 text-dark"><?php echo e(__('Get In Touch')); ?></span>
                    <p class="castrooo"><?php echo e(__('Also find us social media below')); ?></p>
                    <ul class="social-link">
                        <?php $__currentLoopData = $social; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $socials): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if(!empty($socials->value)): ?>
                        <li><a href="<?php echo e($socials->value); ?>" class="icon-<?php echo e($socials->type); ?>"><i class="fab fa-<?php echo e($socials->type); ?>"></i></a></li>
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                    </ul>
                </div>
            </div>
            <div class="col-md-4">
              <div class="services-box-wrapper text-center">
                <div class="my-3 services-box-icon color-1"><i class="fas fa-envelope-open xcolls"></i></div>
                <span class="mb-20 fw-500 text-dark"><?php echo e(__('Email & Phone')); ?></span>
                <p class="castrooo"><?php echo e($set->email); ?><br><?php echo e($set->mobile); ?></p>
              </div>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/kingsmen/core/resources/views/front/index.blade.php ENDPATH**/ ?>