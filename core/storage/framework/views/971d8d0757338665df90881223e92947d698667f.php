<?php $__env->startSection('css'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<section id="header" class="backg backg-one bg-banner-gradient">
    <div class="container">
        <div class="backg-content-wrap">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="backg-content">
                        <span class="discount wow soneFadeUp" data-wosw-delay="0.3s"><?php echo e($set->title); ?></span>
                        <h1 class="backg-title wow soneFadeUp" data-wow-delay="0.5s">
                        <?php echo e(__('Plans that gives assured profits')); ?>

                        </h1><br>
                        <a href="<?php echo e(route('register')); ?>" class="pxs-btn backg-btn wow soneFadeUp" data-wow-delay="0.6s"><?php echo e(__('Get Started')); ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-shape bg-shape-bottom">
        <img src="<?php echo e(url('/')); ?>/asset/images/shape-bg.png">
    </div>
</section>
<section class="pricing-two pt-100 wow soneFadeUp">
    <div class="container">
        <div class="section-title text-center">
            <span class="sub-title"><?php echo e(__('Pricing Plan')); ?></span>
            <h2 class="title">
            <?php echo e(__('Choose your pricing policy which affordable')); ?>

            </h2>
        </div>
        <div class="row advanced-pricing-table no-gutters">
            <?php $__currentLoopData = $plan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="col-lg-3">
                <div class="pricing-table">
                    <div class="pricing-header pricing-amount">
                        <h3 class="price-title"><?php echo e($val->name); ?></h3>
                        <p><?php echo e(__('Payouts wont be availabe till end of plan duration')); ?> </p>
                        <div class="annual_price">
                            <h2 class="price"><?php echo e($currency->symbol.$val->min_deposit); ?></h2>
                        </div>
                        <div class="monthly_price">
                            <h2 class="price"><?php echo e($currency->symbol.number_format($val->min_deposit)); ?></h2>
                        </div>
                        <div class="small_desc text-center">
                            <a href="javascript:void"><?php echo e(__('Profit Topup is Automated')); ?></a><br>
                            <a href="javascript:void"><?php echo e(__('For')); ?> <?php echo e($val->duration); ?> <?php echo e($val->period); ?>(s)</a><br>
                            <a href="javascript:void"><?php echo e($val->percent); ?> <?php echo e(__('Daily Percent')); ?></a><br>
                            <a href="javascript:void"><?php echo e($currency->symbol.number_format($val->amount)); ?> <?php echo e(__('Maximum Price')); ?></a><br>
                            <?php if($val->ref_percent!=null): ?>
                                <p class="text-xs text-dark mb-0"><?php echo e($val->ref_percent); ?>% <?php echo e(__('Referral Percent')); ?></p>
                            <?php endif; ?>                  
                            <?php if($val->bonus!=null): ?>
                                <p class="text-xs text-dark mb-0"><?php echo e($val->bonus); ?>% <?php echo e(__('Trading Bonus')); ?></p>
                            <?php endif; ?>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/kingsmen/core/resources/views/front/plans.blade.php ENDPATH**/ ?>