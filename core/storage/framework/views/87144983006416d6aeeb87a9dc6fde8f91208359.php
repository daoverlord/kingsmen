<?php $__env->startSection('content'); ?>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h6 class="card-title font-weight-semibold"><?php echo e(__('Users')); ?></h6>
                    </div>
                    <div class="">
                        <table class="table datatable-responsive">
                            <thead>
                                <tr>
                                    <th><?php echo e(__('S/N')); ?></th>
                                    <th><?php echo e(__('Full Name')); ?></th>
                                    <th><?php echo e(__('Username')); ?></th>
                                    <th><?php echo e(__('Email')); ?></th>                                                                      
                                    <th><?php echo e(__('Status')); ?></th>
                                    <th><?php echo e(__('Balance')); ?></th>
                                    <th><?php echo e(__('Profit')); ?></th>
                                    <th><?php echo e(__('Referral Bonus')); ?></th>
                                    <th><?php echo e(__('Created')); ?></th>
                                    <th><?php echo e(__('Updated')); ?></th>
                                    <th class="text-center"><?php echo e(__('Action')); ?></th>    
                                </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k=>$val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e(++$k); ?>.</td>
                                    <td><?php echo e($val->first_name); ?> <?php echo e($val->last_name); ?></td>
                                    <td><?php echo e($val->username); ?></td>
                                    <td><?php echo e($val->email); ?></td>
                                    <td>
                                        <?php if($val->status==0): ?>
                                            <span class="badge badge-info"><?php echo e(__('Active')); ?></span>
                                        <?php elseif($val->status==1): ?>
                                            <span class="badge badge-danger"><?php echo e(__('Blocked')); ?></span> 
                                        <?php endif; ?>
                                    </td>   
                                    <td><?php echo e($currency->symbol.number_format($val->balance)); ?></td> 
                                    <td><?php echo e($currency->symbol.number_format($val->profit)); ?></td> 
                                    <td><?php echo e($currency->symbol.number_format($val->ref_bonus)); ?></td> 
                                    <td><?php echo e(date("Y/m/d h:i:A", strtotime($val->created_at))); ?></td>  
                                    <td><?php echo e(date("Y/m/d h:i:A", strtotime($val->updated_at))); ?></td>
                                    <td class="text-center">
                                        <div class="list-icons">
                                            <div class="dropdown">
                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a class='dropdown-item' href="<?php echo e(url('/')); ?>/admin/manage-user/<?php echo e($val->id); ?>"><i class="icon-cogs spinner mr-2"></i><?php echo e(__('Manage account')); ?></a>
                                                    <?php if($val->status==0): ?>
                                                        <a class='dropdown-item' href="<?php echo e(url('/')); ?>/admin/block-user/<?php echo e($val->id); ?>"><i class="icon-eye-blocked2 mr-2"></i><?php echo e(__('Block')); ?></a>
                                                    <?php else: ?>
                                                        <a class='dropdown-item' href="<?php echo e(url('/')); ?>/admin/unblock-user/<?php echo e($val->id); ?>"><i class="icon-eye mr-2"></i><?php echo e(__('Unblock')); ?></a>
                                                    <?php endif; ?>
                                                    <a class='dropdown-item' href="<?php echo e(url('/')); ?>/admin/email/<?php echo e($val->email); ?>/<?php echo e($val->business_name); ?>"><i class="icon-envelope mr-2"></i><?php echo e(__('Send email')); ?></a>    
                                                    <a data-toggle="modal" data-target="#<?php echo e($val->id); ?>delete" class="dropdown-item"><i class="icon-bin2 mr-2"></i><?php echo e(__('Delete account')); ?></a>
                                                </div>
                                            </div>
                                        </div>
                                    </td>                   
                                </tr>
                                <div id="<?php echo e($val->id); ?>delete" class="modal fade" tabindex="-1">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">   
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>
                                            <div class="modal-body">
                                                <h6 class="font-weight-semibold"><?php echo e(__('Are you sure you want to delete this?')); ?></h6>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-link" data-dismiss="modal"><?php echo e(__('Close')); ?></button>
                                                <a  href="<?php echo e(url('/')); ?>/admin/user/delete/<?php echo e($val->id); ?>" class="btn bg-danger"><?php echo e(__('Proceed')); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>               
                            </tbody>                    
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/kingsmen/core/resources/views/admin/user/index.blade.php ENDPATH**/ ?>