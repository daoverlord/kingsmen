
<?php $__env->startSection('css'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<section id="header" class="backg backg-one bg-banner-gradient">
    <div class="container">
        <div class="backg-content-wrap">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="backg-content">
                        <span class="discount wow soneFadeUp" data-wosw-delay="0.3s"><?php echo e($set->title); ?></span>
                        <h1 class="backg-title wow soneFadeUp" data-wow-delay="0.5s"><?php echo e(__('Contact us')); ?></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-shape bg-shape-bottom">
        <img src="<?php echo e(url('/')); ?>/asset/images/shape-bg.png">
    </div>
</section>
<section id="contact" class="wow soneFadeUp" data-wow-delay="0.3s">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="contact-froms">
                    <form action="<?php echo e(route('contact-submit')); ?>" method="post" class="contact-form" data-saasone="contact-froms">
                    <?php echo csrf_field(); ?>
                        <div class="row">
                            <div class="col-md-12">
                            <input type="text" name="name" placeholder="Name" required>
                            <input type="text" name="mobile" placeholder="Mobile" required>
                            </div>
                        </div>
                        <input type="email" name="email" placeholder="Email" required>
                        <textarea name="message" placeholder="Your Message" required></textarea> 

                        <button type="submit" class="sone-btn"><?php echo e(__('Send')); ?></button>
                        
                        <div class="form-result alert">
                            <div class="content"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="services address-contact">
    <div class="container">
        <div class="section-title text-center">
            <h2 class="title"><?php echo e(__('Don’t hesitate to contact us for')); ?><br><?php echo e(__('any information')); ?></h2>
        </div>
        <div class="row gap-y">
            <div class="col-md-4">
                <div class="services-box-wrapper text-center">
                    <div class="my-3 services-box-icon color-1"><i class="fas fa-map-marker-alt xcolls"></i></div>
                    <span class="mb-20 fw-500 text-dark"><?php echo e(__('Our Location')); ?></span>
                    <p class="castrooo"><?php echo e($set->address); ?></p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="services-box-wrapper text-center">
                    <div class="my-3 services-box-icon color-1"><i class="fas fa-border-all xcolls"></i></div>
                    <span class="mb-20 fw-500 text-dark"><?php echo e(__('Get In Touch')); ?></span>
                    <p class="castrooo"><?php echo e(__('Also find us social media below')); ?></p>
                    <ul class="social-link">
                        <?php $__currentLoopData = $social; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $socials): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if(!empty($socials->value)): ?>
                        <li><a href="<?php echo e($socials->value); ?>" class="icon-<?php echo e($socials->type); ?>"><i class="fab fa-<?php echo e($socials->type); ?>"></i></a></li>
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                    </ul>
                </div>
            </div>
            <div class="col-md-4">
              <div class="services-box-wrapper text-center">
                <div class="my-3 services-box-icon color-1"><i class="fas fa-envelope-open xcolls"></i></div>
                <span class="mb-20 fw-500 text-dark"><?php echo e(__('Email & Phone')); ?></span>
                <p class="castrooo"><?php echo e($set->email); ?><br><?php echo e($set->mobile); ?></p>
              </div>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/kingsmen/core/resources/views/front/contact.blade.php ENDPATH**/ ?>