<?php $__env->startSection('css'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<section id="header" class="backg backg-one bg-banner-gradient">
  <div class="container">
    <div class="backg-content-wrap">
      <div class="row align-items-center">
        <div class="col-lg-6 z100">
          <div class="backg-content">
            <span class="discount wow soneFadeUp" data-wosw-delay="0.3s"><?php echo e(__('Welcome back,')); ?></span>
            <h1 class="backg-title wow soneFadeUp" data-wow-delay="0.5s"><?php echo e(__('Sign in to continue')); ?></h1>     
            <span class="text-small"><?php echo e(__('Trouble signing in? ')); ?><a href="mailto:<?php echo e($set->email); ?>"><?php echo e(__('contact support')); ?></a></span>             
          </div>
        </div>
        <div class="col-lg-6">
          <div class="wow soneFadeLeft">
            <div class="pt-100"></div>
            <form action="<?php echo e(route('submitlogin')); ?>" method="post" class="contact-form" data-saasone="contact-froms">
                <?php echo csrf_field(); ?>
              <input type="email" name="email" placeholder="Email" required>
              <input type="password" name="password" placeholder="Password" required>
              <div class="text-left">
                <a href="<?php echo e(route('user.password.request')); ?>"><span class="text-small"><?php echo e(__('Forgot password?')); ?></span></a>
              </div>                              
              <div class="text-right">
                <button type="submit" class="sone-btn"><?php echo e(__('Sign In')); ?></button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/kingsmen/core/resources/views//auth/login.blade.php ENDPATH**/ ?>