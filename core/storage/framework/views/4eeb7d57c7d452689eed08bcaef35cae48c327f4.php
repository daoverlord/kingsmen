<?php $__env->startSection('content'); ?>
<div class="content"> 
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h6 class="card-title font-weight-semibold"><?php echo e(__('Send email')); ?></h6>
                </div>
                <div class="card-body">
                    <form action="<?php echo e(route('user.promo.send')); ?>" method="post">
                    <?php echo csrf_field(); ?>
                        <div class="form-group row">
                            <label class="col-form-label col-lg-2"><?php echo e(__('To')); ?></label>
                            <div class="col-lg-10">
                            <select multiple="multiple" class="form-control select" name="email[]" data-fouc>
                                <optgroup label="Subscribed users">
                                    <?php $__currentLoopData = $client; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($val->email); ?>" selected><?php echo e($val->email); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </optgroup>
                            </select>
                            </div>
                        </div>                                              
                        <div class="form-group row">
                            <label class="col-form-label col-lg-2"><?php echo e(__('Subject')); ?></label>
                            <div class="col-lg-10">
                                <input type="text" name="subject" maxlength="200" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-lg-2"><?php echo e(__('Message')); ?></label>
                            <div class="col-lg-10">
                                <textarea type="text" name="message" rows="10" class="form-control tinymce"></textarea>
                            </div>
                        </div>          
                    <div class="text-right">
                        <button type="submit" class="btn btn-success btn-sm"><?php echo e(__('Send')); ?></button>
                    </div>
                </form>
            </div>
        </div>    
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/kingsmen/core/resources/views/admin/user/promo.blade.php ENDPATH**/ ?>