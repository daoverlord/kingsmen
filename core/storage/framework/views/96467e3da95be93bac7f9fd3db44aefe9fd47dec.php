<?php $__env->startSection('content'); ?>
<div class="content">
<div class="row">
  <div class="col-md-4">
    <div class="card">
      <div class="card-body">
        <div class="d-sm-flex align-item-sm-center flex-sm-nowrap">
          <div>
            <h6 class="font-weight-semibold"><?php echo e(__('Users')); ?></h6>
            <ul class="list list-unstyled mb-0">
              <li><?php echo e(__('Active users:')); ?> <span class="font-weight-semibold text-default">#<?php echo e($activeusers); ?></span></li>
              <li><?php echo e(__('Blocked users:')); ?> <span class="font-weight-semibold text-default">#<?php echo e($blockedusers); ?></span></li>
            </ul>
          </div>

          <div class="text-sm-right mb-0 mt-3 mt-sm-0 ml-auto">
            <h3 class="font-weight-semibold">#<?php echo e($totalusers); ?></h3>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="card">
      <div class="card-body">
        <div class="d-sm-flex align-item-sm-center flex-sm-nowrap">
          <div>
            <h6 class="font-weight-semibold"><?php echo e(__('Support Ticket')); ?></h6>
            <ul class="list list-unstyled mb-0">
              <li><?php echo e(__('Open tickets:')); ?> <span class="font-weight-semibold text-default">
                #<?php echo e($openticket); ?></span></li>
              <li><?php echo e(__('Closed tickets:')); ?> <span class="font-weight-semibold text-default">
                #<?php echo e($closedticket); ?></span>
              </li>
            </ul>
          </div>
          <div class="text-sm-right mb-0 mt-3 mt-sm-0 ml-auto">
            <h3 class="font-weight-semibold">#<?php echo e($totalticket); ?></h3>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="card">
      <div class="card-body">
        <div class="d-sm-flex align-item-sm-center flex-sm-nowrap">
          <div>
            <h6 class="font-weight-semibold"><?php echo e(__('Platform Reviews')); ?></h6>
            <ul class="list list-unstyled mb-0">
              <li><?php echo e(__('Published reviews:')); ?> <span class="font-weight-semibold text-default">
                #<?php echo e($pubreview); ?></span></li>
              <li><?php echo e(__('Pending reviews:')); ?> <span class="font-weight-semibold text-default">
                #<?php echo e($unpubreview); ?></span>
              </li>
            </ul>
          </div>
          <div class="text-sm-right mb-0 mt-3 mt-sm-0 ml-auto">
            <h3 class="font-weight-semibold">#<?php echo e($totalreview); ?></h3>
          </div>
        </div>
      </div>
    </div>
  </div>  
  <div class="col-md-4">
    <div class="card">
      <div class="card-body">
        <div class="d-sm-flex align-item-sm-center flex-sm-nowrap">
          <div>
            <h6 class="font-weight-semibold"><?php echo e(__('Other Deposits')); ?></h6>
            <ul class="list list-unstyled mb-0">
              <li><?php echo e(__('Pending:')); ?> <span class="font-weight-semibold text-default">
                #<?php echo e($pendingdep); ?></span></li>
              <li><?php echo e(__('Approved:')); ?> <span class="font-weight-semibold text-default">
                #<?php echo e($approveddep); ?></span>
              </li>              
              <li><?php echo e(__('Declined:')); ?> <span class="font-weight-semibold text-default">
                #<?php echo e($declineddep); ?></span>
              </li>
            </ul>
          </div>
          <div class="text-sm-right mb-0 mt-3 mt-sm-0 ml-auto">
            <h3 class="font-weight-semibold">#<?php echo e($totaldeposit); ?></h3>
          </div>
        </div>
      </div>
    </div>
  </div>  
  <div class="col-md-4">
    <div class="card">
      <div class="card-body">
        <div class="d-sm-flex align-item-sm-center flex-sm-nowrap">
          <div>
            <h6 class="font-weight-semibold"><?php echo e(__('Withdrawal')); ?></h6>
            <ul class="list list-unstyled mb-0">
              <li><?php echo e(__('Pending:')); ?> <span class="font-weight-semibold text-default">
                #<?php echo e($pendingwd); ?></span></li>
              <li><?php echo e(__('Approved:')); ?> <span class="font-weight-semibold text-default">
                #<?php echo e($approvedwd); ?></span>
              </li>              
              <li><?php echo e(__('Declined:')); ?> <span class="font-weight-semibold text-default">
                #<?php echo e($declinedwd); ?></span>
              </li>
            </ul>
          </div>
          <div class="text-sm-right mb-0 mt-3 mt-sm-0 ml-auto">
            <h3 class="font-weight-semibold">#<?php echo e($totalwd); ?></h3>
          </div>
        </div>
      </div>
    </div>
  </div>   
  <div class="col-md-4">
    <div class="card">
      <div class="card-body">
        <div class="d-sm-flex align-item-sm-center flex-sm-nowrap">
          <div>
            <h6 class="font-weight-semibold"><?php echo e(__('Investment plans')); ?></h6>
            <ul class="list list-unstyled mb-0">
              <li><?php echo e(__('Active:')); ?> <span class="font-weight-semibold text-default">
                #<?php echo e($appplan); ?></span></li>
              <li><?php echo e(__('Disabled:')); ?> <span class="font-weight-semibold text-default">
                #<?php echo e($penplan); ?></span>
              </li>              
            </ul>
          </div>
          <div class="text-sm-right mb-0 mt-3 mt-sm-0 ml-auto">
            <h3 class="font-weight-semibold">#<?php echo e($totalplan); ?></h3>
          </div>
        </div>
      </div>
    </div>
  </div>  
  <div class="col-md-4">
    <div class="card">
      <div class="card-body">
        <div class="d-sm-flex align-item-sm-center flex-sm-nowrap">
          <div>
            <h6 class="font-weight-semibold"><?php echo e(__('Investment')); ?></h6>
            <ul class="list list-unstyled mb-0">
              <li><?php echo e(__('Active:')); ?> <span class="font-weight-semibold text-default">
                #<?php echo e($appprofit); ?></span></li>
              <li><?php echo e(__('Completed:')); ?> <span class="font-weight-semibold text-default">
                #<?php echo e($penprofit); ?></span>
              </li>              
            </ul>
          </div>
          <div class="text-sm-right mb-0 mt-3 mt-sm-0 ml-auto">
            <h3 class="font-weight-semibold">#<?php echo e($totalprofit); ?></h3>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php $__env->stopSection(); ?>
<?php echo $__env->make('master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/kingsmen/core/resources/views/admin/dashboard/index.blade.php ENDPATH**/ ?>