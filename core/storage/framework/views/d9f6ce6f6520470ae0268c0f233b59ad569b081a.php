<?php $__env->startSection('content'); ?>
<div class="container-fluid mt--6">
  <div class="content-wrapper">
  <div class="row">
    <div class="col-lg-8">
      <div class="row">
          <div class="col-lg-6">
            <div class="card bg-future">
              <!-- Card header -->
              <div class="card-header">
                <div class="row align-items-center">
                  <div class="col-8">
                    <!-- Surtitle -->
                    <h5 class="surtitle text-future"><?php echo e(__('Last 5 trades')); ?></h5>
                    <!-- Title -->
                    <h5 class="h3 mb-0 text-dark"><?php echo e(__('Progress track')); ?></h5>
                  </div>
                </div>
              </div>
              <!-- Card body -->
              <div class="card-body">
                <!-- List group -->
                <ul class="list-group list-group-flush list my--3">
                  <?php $__currentLoopData = $profit; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k=>$val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <li class="list-group-item px-0">
                    <div class="row align-items-center">
                      <div class="col">
                        <h5 class="text-dark">#<?php echo e($val->trx); ?> @ <?php echo e($val->plan->name); ?> [<?php echo e(number_format($val->profit)); ?>/<?php echo e(number_format($val->amount).$currency->name); ?>]</h5>
                        <div class="progress progress-xs mb-0">
                          <div class="progress-bar bg-progress" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo e(($val->profit*100)/$val->amount); ?>%;"></div>
                        </div>
                      </div>
                    </div>
                  </li>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
              </div>
            </div>
            <div class="card bg-future border-0">
              <div class="card-body">
                <div class="row">
                  <div class="col">
                    <h4 class="card-title mb-0"><?php echo e(__('Available Profit')); ?></h4>
                    <span class="mb-0 text-future"><?php echo e($currency->symbol.number_format($user->profit)); ?></span>
                  </div>
                </div>
              </div>
            </div>
            <div class="card bg-future border-0">
              <div class="card-body">
                <div class="row">
                  <div class="col">
                    <h4 class="card-title mb-0"><?php echo e(__('Referral Earnings')); ?></h4>
                    <span class="mb-0 text-future"><?php echo e($currency->symbol.number_format($user->ref_bonus)); ?></span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="card bg-future">
              <div class="card-body">
                <div class="row align-items-center">
                  <div class="col text-center">
                    <h4 class="mb-4 text-future">
                    <?php echo e(__('Statistics')); ?>

                    </h4>
                    <span class="text-sm text-dark mb-0"><i class="fa fa-google-wallet"></i><?php echo e(__(' Received')); ?></span><br>
                    <span class="text-xl text-dark mb-0"><?php echo e($currency->name); ?> <?php echo e(number_format($received)); ?>.00</span><br>
                    <hr>
                  </div>
                </div>
                <div class="row align-items-center">
                  <div class="col">
                    <div class="my-4">
                      <span class="surtitle"><?php echo e(__('Pending')); ?></span><br>
                      <span class="surtitle "><?php echo e(__('Total')); ?></span>
                    </div>
                  </div>
                  <div class="col-auto">
                    <div class="my-4">
                      <span class="surtitle "><?php echo e($currency->name); ?> <?php echo e(number_format($pending)); ?>.00</span><br>
                      <span class="surtitle text-future"><?php echo e($currency->name); ?> <?php echo e(number_format($total)); ?>.00</span>
                    </div>
                  </div>
                </div>

              </div>
            </div>
            <div class="pricing card-group flex-column flex-md-row mb-3">
              <div class="card card-pricing border-0 bg-future text-center mb-4">
                <a href="#" data-toggle="modal" data-target="#buy<?php echo e($plan->id); ?>">
                  <div class="card-body px-lg-7">
                    <p class="card-text text-xs text-dark"><?php echo e(__('Payouts wont be availabe till end of plan duration. Interest means profit and compound is sum of money invested plus profit. Trading bonus is a certain percent of your compound interest.')); ?></p>
                    <h4 class="card-title mb-0"><?php echo e(__('Most Purchased')); ?></h4>
                    <div class="display-2 text-future"><?php echo e($currency->symbol.number_format($plan->min_deposit)); ?></div>
                    <p class="card-text text-sm text-dark text-uppercase"><?php echo e(__('For')); ?>  <?php echo e($plan->duration.' '.$plan->period); ?>(s)</p>
                    <p class="text-xs text-dark mb-0"><?php echo e($plan->percent); ?>% <?php echo e(__('Daily Top Up')); ?></p>
                    <p class="text-xs text-dark mb-0"><?php echo e(__('Maximum Price')); ?> - <?php echo e($currency->symbol.number_format($plan->amount)); ?> </p>
                    <p class="text-xs text-dark mb-0"><?php echo e(__('Interest')); ?> <?php echo e(($plan->percent*castrotime($plan->duration.' '.$plan->period))-100); ?>%</p>
                    <p class="text-xs text-dark mb-0"><?php echo e(__('Compound Interest')); ?>  <?php echo e($plan->percent*castrotime($plan->duration.' '.$plan->period)); ?>%</p>
                    <?php if($plan->ref_percent!=null): ?>
                      <p class="text-xs text-dark mb-0"><?php echo e($plan->ref_percent); ?>% <?php echo e(__('Referral Percent')); ?></p>
                    <?php endif; ?>                  
                    <?php if($plan->bonus!=null): ?>
                      <p class="text-xs text-dark mb-0"><?php echo e($plan->bonus); ?>% <?php echo e(__('Trading Bonus')); ?></p>
                    <?php endif; ?>
                  </div>
                </a>
              </div>
              <div class="modal fade" id="buy<?php echo e($plan->id); ?>" tabindex="-1" role="dialog" aria-labelledby="modal-form" aria-hidden="true">
                <div class="modal-dialog modal- modal-dialog-centered modal-sm" role="document">
                  <div class="modal-content">
                    <div class="modal-body p-0">
                      <div class="card bg-progress border-0 mb-0">
                        <div class="card-header bg-transparent pb-5">
                          <div class="text-white text-center mt-2 mb-3"><small><?php echo e(__('Purchase plan')); ?></small></div>
                          <div class="btn-wrapper text-center">
                              <h4 class="text-uppercase ls-1 text-white py-3 mb-0"><?php echo e($plan->name); ?></h4>
                          </div>
                        </div>
                        <div class="card-body">
                          <form role="form" action="<?php echo e(url('user/buy')); ?>" method="post">
                          <?php echo csrf_field(); ?>
                            <div class="form-group mb-3">
                              <div class="input-group input-group-merge input-group-alternative">
                                <div class="input-group-prepend">
                                  <span class="input-group-text"><?php echo e($currency->symbol); ?></span>
                                </div>
                                <input type="number" step="any" class="form-control" placeholder="" name="amount" required>
                                <input type="hidden" name="plan" value="<?php echo e($plan->id); ?>">
                              </div>
                            </div>
                            <div class="text-center">
                              <button type="submit" class="btn btn-success btn-sm my-4"><?php echo e(__('Purchase')); ?></button>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
    </div>
    <div class="col-lg-4">
      <div class="card bg-future border-0">
        <div class="card-body">
          <div class="row">
            <div class="col">
              <h3 class="card-title mb-1"><?php echo e(__('Two Factor Authentication')); ?></h3>
              <span class="badge badge-pill badge-future">
                <?php if($user->fa_status==0): ?>
                <?php echo e(__('Disabled')); ?>

                <?php else: ?>
                <?php echo e(__('Active')); ?>

                <?php endif; ?>
              </span>
            </div>
          </div>
        </div>
      </div>
      <?php if($set->kyc): ?>
        <div class="card bg-future" id="kyc">
          <div class="card-body">
            <h3 class="card-title mb-3"><?php echo e(__('Identity verification')); ?></h3>
            <p class="card-text text-xs text-dark"><?php echo e(__('Please Upload an Identity Document, for example, Driver Licence, Voters Card, International Passport, National ID.')); ?></p>
            <span class="badge badge-pill badge-future mb-3">
              <?php if($user->kyc_status==0): ?>
              <?php echo e(__('Under Review')); ?>

              <?php else: ?>
              <?php echo e(__('Verified')); ?>

              <?php endif; ?>
            </span>
            <?php if(empty($user->kyc_link)): ?>
                <form method="post" action="<?php echo e(url('user/kyc')); ?>" enctype="multipart/form-data">
                <?php echo csrf_field(); ?>
                    <div class="form-group">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" id="customFileLang1" name="image" lang="en">
                        <label class="custom-file-label" for="customFileLang1"><?php echo e(__('Select document')); ?></label>
                      </div>
                    </div>
                  <div class="text-right">
                    <input type="submit" class="btn btn-success btn-sm" value="Upload">
                  </div>
                </form>
            <?php endif; ?>
          </div>
        </div>
      <?php endif; ?>
      <?php if($set->referral==1): ?>
      <div class="card bg-future">
        <div class="card-body">
          <h3 class="card-title mb-3"><?php echo e(__('Referral link')); ?></h3>
          <p class="card-text text-xs text-dark"><?php echo e(__('Automatically Top up your Balance by Sharing your Referral Link, Earn a percentage of whatever Plan your Referred user Buys.')); ?></p>
          <span class="form-text text-xs"><?php echo e(url('/')); ?>/referral/<?php echo e($user->username); ?></span>
          <button type="button" class="btn-icon-clipboard" data-clipboard-text="<?php echo e(url('/')); ?>/referral/<?php echo e($user->username); ?>" title="Copy"><?php echo e(__('Copy')); ?></button>
        </div>
      </div>
      <?php endif; ?>
  </div>  
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('userlayout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/kingsmen/core/resources/views/user/index.blade.php ENDPATH**/ ?>