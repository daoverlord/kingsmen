<?php $__env->startSection('content'); ?>
<div class="container-fluid mt--6">
  <div class="content-wrapper">
    <div class="row">  
      <div class="col-lg-8">
        <div class="row">
          <?php $__currentLoopData = $earning; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k=>$val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="col-md-12">
                <div class="card bg-future">
                  <!-- Card body -->
                  <div class="card-body">
                    <div class="row align-items-center">
                      <div class="col-12">
                        <!-- Title -->
                        <h5 class="h4 mb-0">#<?php echo e($val->ref_id); ?></h5>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col">
                        <p class="text-xs text-dark mb-0"><?php echo e(__('Amount:')); ?> <?php echo e($currency->symbol.number_format($val->amount)); ?></p>
                        <p class="text-xs text-dark mb-0"><?php echo e(__('From:')); ?> <?php echo e($val->user['first_name']); ?> <?php echo e($val->user['last_name']); ?></p>
                        <p class="text-xs text-dark mb-0"><?php echo e(__('Plan:')); ?> <?php echo e($val->plan['name']); ?></p>
                        <p class="text-xs text-dark mb-0"><?php echo e(__('Created:')); ?> <?php echo e(date("Y/m/d h:i:A", strtotime($val->created_at))); ?></p>
                        <p class="text-xs text-dark mb-0"><?php echo e(__('Updated:')); ?> <?php echo e(date("Y/m/d h:i:A", strtotime($val->updated_at))); ?></p>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="card">
          <div class="card-header">
            <h5 class="h3 mb-0"><?php echo e(__('Referrals')); ?></h5>
          </div>
          <div class="card-body">
            <ul class="list-group list-group-flush list my--3">
              <?php $__currentLoopData = $referral; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k=>$val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <li class="list-group-item px-0">
                <div class="row align-items-center">
                  <div class="col">
                    <h4 class="mb-0">
                      <a href="javascript:void;"><?php echo e($val->user['first_name']); ?> <?php echo e($val->user['last_name']); ?></a>
                    </h4>
                    <small><?php echo e($val->user['username']); ?> @ <?php echo e(date("Y/m/d h:i:A", strtotime($val->created_at))); ?></small>
                  </div>
                </div>
              </li>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
          </div>
        </div>
      </div>
      </div> 
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('userlayout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/kingsmen/core/resources/views/user/profile/referral.blade.php ENDPATH**/ ?>