<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Models\User;
use App\Models\Settings;
use App\Models\Logo;
use App\Models\Currency;
use App\Models\Plans;
use App\Models\Gateway;
use App\Models\Deposits;
use App\Models\Withdraw;
use App\Models\Withdrawm;
use App\Models\Profits;
use App\Models\Ticket;
use App\Models\Reply;
use App\Models\Referral;
use App\Models\Earning;
use App\Models\Transfer;
use App\Models\Adminbank;
use App\Models\Banktransfer;
use App\Models\Bank;
use Carbon\Carbon;
use Session;
use Image;




class UserController extends Controller
{        
    public function __construct()
    {		
        $this->middleware('auth');
    }
   
//Support ticket
    public function ticket()
    {
        $data['title']='Tickets';
        $data['ticket']=Ticket::whereUser_id(Auth::user()->id)->get();
        return view('user.support.index', $data);
    } 
    public function Replyticket($id)
    {
        $data['ticket']=$ticket=Ticket::find($id);
        $data['title']='#'.$ticket->ticket_id;
        $data['reply']=Reply::whereTicket_id($ticket->ticket_id)->get();
        return view('user.support.reply', $data);
    }  
    public function Destroyticket($id)
    {
        $data = Ticket::findOrFail($id);
        $res =  $data->delete();
        if ($res) {
            return back()->with('success', 'Request was Successfully deleted!');
        } else {
            return back()->with('alert', 'Problem With Deleting Request');
        }
    } 
    public function submitticket(Request $request)
    {
        $set=Settings::first();
        $user=$data['user']=User::find(Auth::user()->id);
        $sav['user_id']=Auth::user()->id;
        $sav['subject']=$request->subject;
        $sav['priority']=$request->category;
        $sav['message']=$request->details;
        $sav['ticket_id']=$token=str_random(16);
        $sav['status']=0;
        Ticket::create($sav);
        if($set['email_notify']==1){
            send_email($user->email, $user->username, 'New Ticket:'. $token, "Thank you for contacting us, we will get back to you shortly");
            send_email($set->email, $set->site_name, 'New Ticket:'. $token, "New ticket request");
        }
        return back()->with('success', 'Ticket Submitted Successfully.');
    }     
    public function submitreply(Request $request)
    {
        $set=Settings::first();
        $sav['reply']=$request->details;
        $sav['ticket_id']=$request->id;
        $sav['status']=1;
        Reply::create($sav);
        if($set['email_notify']==1){
            send_email($set->email, $set->site_name, 'Ticket Reply:'. $request->id, "New ticket reply request");
        }
        $data=Ticket::whereTicket_id($request->id)->first();
        $data->status=0;
        $data->save();
        return back()->with('success', 'Message sent!.');
    }   
//End Support ticket

//Fund account 
    public function fund()
    {
        $data['title']='Fund account';
        $data['adminbank']=Adminbank::whereId(1)->first();
        $data['gateways']=Gateway::whereStatus(1)->orderBy('id', 'DESC')->get();
        $data['deposits']=Deposits::whereUser_id(Auth::user()->id)->latest()->get();
        $data['bank_transfer']=Banktransfer::whereUser_id(Auth::user()->id)->orderBy('id', 'DESC')->get();
        return view('user.fund.index', $data);
    }
        
    public function bank_transfer()
    {
        $data['title']='Bank transfer';
        $data['bank']=Adminbank::whereId(1)->first();
        return view('user.fund.bank', $data);
    }

    public function bank_transfersubmit(Request $request)
    {
        $user=$data['user']=User::find(Auth::user()->id);
        $currency=Currency::whereStatus(1)->first();
        $set=Settings::first();
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $filename = time() . '_' . $user->username . '.jpg';
            $location = 'asset/screenshot/' . $filename;
            Image::make($image)->resize(800, 800)->save($location);
            $sav['user_id']=Auth::user()->id;
            $sav['amount']=$request->amount;
            $sav['details']=$request->details;
            $sav['image']=$filename;
            $sav['status'] = 0;
            $sav['trx']=$trx=str_random(16);
            Banktransfer::create($sav);
            if($set['email_notify']==1){
                send_email($user->email,$user->username,'Deposit request under review','We are currently reviewing your deposit of '.$request->amount.$currency->name.', once confirmed your balance will be credited automatically.<br>Thanks for working with us.');    			
                send_email($set->email,$set->site_name,'New bank deposit request','Hello admin, you have a new bank deposit request for '.$trx);
            }
            return redirect()->route('user.fund')->with('success', 'Deposit request under review');
        }else{
            return back()->with('warning', 'An error occured, please try again later');
        }
    } 
    public function fundsubmit(Request $request)
    {
        $gate=Gateway::where('id', $request->gateway)->first();
        $user=User::find(Auth::user()->id);
        if ($gate->minamo <= $request->amount && $gate->maxamo >= $request->amount) {
            $charge = $gate->fixed_charge + ($request->amount * $gate->percent_charge / 100);
            $usdamo = ($request->amount + $charge) / $gate->rate;
            $usdamo = round($usdamo, 2);
            $trx = round(microtime(true));
            $depo['user_id'] = Auth::id();
            $depo['gateway_id'] = $gate->id;
            $depo['amount'] = $request->amount + $charge;
            $depo['charge'] = $charge;
            $depo['usd'] = round($usdamo, 2);
            $depo['btc_amo'] = 0;
            $depo['btc_wallet'] = "";
            $depo['trx'] = str_random(16);
            $depo['try'] = 0;
            $depo['status'] = 0;
            Deposits::create($depo);
            Session::put('Track', $depo['trx']);
            return redirect()->route('user.preview');        
        } else {
            return back()->with('alert', 'Please Follow Deposit Limit');
        }
    }
    
    public function depositpreview()
    {
        $track = Session::get('Track');
        $data['title']='Deposit Preview';
        $data['gate'] = Deposits::where('status', 0)->where('trx', $track)->first();
        return view('user.fund.preview', $data);
    } 
//End of Fund Account
   
//Withdrawal
    public function withdraw()
    {
        $data['title']='Withdraw';
        $data['method']=Withdrawm::whereStatus(1)->get();
        $data['withdraw']=Withdraw::whereUser_id(Auth::user()->id)->orderBy('id', 'DESC')->get();
        $data['received']=Withdraw::whereStatus(1)->whereuser_id(Auth::user()->id)->sum('amount');
        $data['pending']=Withdraw::whereStatus(0)->whereuser_id(Auth::user()->id)->sum('amount');
        $data['total']=Withdraw::whereuser_id(Auth::user()->id)->sum('amount');
        return view('user.profile.withdraw', $data);
    }   
    public function withdrawupdate(Request $request)
    {
        $withdraw=Withdraw::whereId($request->withdraw_id)->first();
        $withdraw->coin_id=$request->coin;
        $withdraw->details=$request->details;
        $withdraw->save();
        return back()->with('success', 'Successfully updated');
    } 
    public function withdrawsubmit(Request $request)
    {
        $set=$data['set']=Settings::first();
        $user=$data['user']=User::find(Auth::user()->id);
        $currency=Currency::whereStatus(1)->first();
        $amount=$request->amount-($request->amount*$set->withdraw_charge/100);
        $token=str_random(16);
        if($request->type==1){
            if($user->profit>$amount || $user->profit==$amount){
                $sav['user_id']=Auth::user()->id;
                $sav['amount']=$amount;
                $sav['charge']=$request->amount*$set->withdraw_charge/100;
                $sav['status']=0;
                $sav['details']=$request->details;
                $sav['type']=$request->type;
                $sav['coin_id']=$request->coin;
                $sav['reference']=$token;
                Withdraw::create($sav);
                $user->profit=$user->profit-$amount;
                $user->save();
                if($set->email_notify==1){
                    send_email(
                        $user->email, 
                        $user->username, 
                        'Withdraw Request currently being Processed', 
                        'We are currently reviewing your withdrawal request of '.$request->amount.$currency->name.'.<br>Thanks for working with us.'
                    );
                    send_email(
                        $set->email, 
                        $set->site_name, 
                        'New Withdraw Request', 
                        'You currently have a new withdrawal request of '.$currency->symbol.$request->amount.'.<br>#'.$token.''
                    );
                }
                return back()->with('success', 'Withdrawal request has been submitted, you will be updated shortly.');
            }else{
                return back()->with('alert', 'Insufficent balance.');
            }
        }elseif($request->type==2){
            if($user->balance>$amount || $user->balance==$amount){
                $sav['user_id']=Auth::user()->id;
                $sav['amount']=$amount;
                $sav['charge']=$request->amount*$set->withdraw_charge/100;
                $sav['status']=0;
                $sav['details']=$request->details;
                $sav['type']=$request->type;
                $sav['coin_id']=$request->coin;
                $sav['reference']=$token;
                Withdraw::create($sav);
                $user->balance=$user->balance-$amount;
                $user->save();
                if($set->email_notify==1){
                    send_email(
                        $user->email, 
                        $user->username, 
                        'Withdraw Request currently being Processed', 
                        'We are currently reviewing your withdrawal request of '.$request->amount.$currency->name.'.<br>Thanks for working with us.'
                    );
                    send_email(
                        $set->email, 
                        $set->site_name, 
                        'New Withdraw Request', 
                        'You currently have a new withdrawal request of '.$currency->symbol.$request->amount.'.<br>#'.$token.''
                    );
                }
                return back()->with('success', 'Withdrawal request has been submitted, you will be updated shortly.');
            }else{
                return back()->with('alert', 'Insufficent balance.');
            }
        }elseif($request->type==3){
            if($user->ref_bonus>$amount || $user->ref_bonus==$amount){
                $sav['user_id']=Auth::user()->id;
                $sav['amount']=$amount;
                $sav['charge']=$request->amount*$set->withdraw_charge/100;
                $sav['status']=0;
                $sav['details']=$request->details;
                $sav['type']=$request->type;
                $sav['coin_id']=$request->coin;
                $sav['reference']=$token;
                Withdraw::create($sav);
                $user->ref_bonus=$user->ref_bonus-$amount;
                $user->save();
                if($set->email_notify==1){
                    send_email(
                        $user->email, 
                        $user->username, 
                        'Withdraw Request currently being Processed', 
                        'We are currently reviewing your withdrawal request of '.$request->amount.$currency->name.'.<br>Thanks for working with us.'
                    );
                    send_email(
                        $set->email, 
                        $set->site_name, 
                        'New Withdraw Request', 
                        'You currently have a new withdrawal request of '.$currency->symbol.$request->amount.'.<br>#'.$token.''
                    );
                }
                return back()->with('success', 'Withdrawal request has been submitted, you will be updated shortly.');
            }else{
                return back()->with('alert', 'Insufficent balance.');
            }
        }
    }  
//End of Withdrawaal

//Transfer
    public function submitownbank(Request $request)
    {
        $set=Settings::first();
        $currency=Currency::whereStatus(1)->first();
        $amountx=$request->amount+($request->amount*$set->transfer_charge/100);
        $user=$data['user']=User::find(Auth::user()->id);
        if($user->email!=$request->email){
                if($user->balance>$amountx || $user->balance==$amountx){
                    Session::put('Amount', $request->amount);
                    Session::put('Acctemail', $request->email);
                    return redirect()->route('user.localpreview'); 
                }else{
                    return back()->with('alert', 'Account balance is insufficient');
                }
        }else{
            return back()->with('alert', 'You cant transfer money to the same account.');
        }
    } 
    public function submitlocalpreview(Request $request)
    {
        $set=Settings::first();
        $currency=Currency::whereStatus(1)->first();
        $amountx=$request->amount+($request->amount*$set->transfer_charge/100);
        $user=$data['user']=User::find(Auth::user()->id);
        $check=User::whereEmail($request->email)->get();
        $user->balance=$user->balance-$amountx;
        $user->save();
        $token=str_random(16);
        if(count($check)>0){
            $trans=User::whereEmail($request->email)->first();
            $sav['ref_id']=$token;
            $sav['amount']=$request->amount;
            $sav['charge']=($request->amount*$set->transfer_charge/100);
            $sav['sender_id']=Auth::user()->id;
            $sav['receiver_id']=$trans->id;        
            $sav['status']=1;   
            Transfer::create($sav);     
            $trans->balance=$trans->balance+$request->amount;
            $trans->save(); 
            $contentx='Email:'.$trans->email.', Date:'.Carbon::now().', CR Amt:'.$request->amount.',
            Bal:'.$trans->balance.', Ref:'.$token.', Desc: Bank transfer';  
            $content='Email:'.$user->email.', Date:'.Carbon::now().', DR Amt:'.$request->amount.',
            Bal:'.$user->balance.', Ref:'.$token.', Desc: Bank transfer'; 
            if($set['email_notify']==1){
                send_email($trans->email, $trans->username, 'Credit alert', $contentx);
                send_email($user->email, $user->username, 'Debit alert', $content);
            }
            return redirect()->route('user.ownbank')->with('success', 'Transfer was successful');
        }else{
            $sav['ref_id']=$token;
            $sav['amount']=$request->amount-($request->amount*$set->transfer_charge/100);
            $sav['charge']=($request->amount*$set->transfer_charge/100);
            $sav['sender_id']=Auth::user()->id;  
            $sav['temp']=$request->email;  
            $sav['status']=0; 
            Transfer::create($sav);   
            $content='Email:'.$user->email.', Date:'.Carbon::now().', DR Amt:'.$request->amount.',
            Bal:'.$user->balance.', Ref:'.$token.', Desc: Bank transfer'; 
            if($set['email_notify']==1){
                send_email($request->email, $set->full_name, 'Confirm transaction'.$token, 'You are receiving this email because a user of '.$set->site_name.', sent '.$currency->symbol.$request->amount.' to this email, but no account was found with this email, click button link to register with this email and confirm transaction, <a href="'.url('/').'/register">Register</a>');
                send_email($user->email, $user->username, 'Debit alert', $content);
            }
            return redirect()->route('user.ownbank')->with('success', 'Transfer was successful, but user has to create account to confirm transaction');
        }
    }     
    public function Receivedpay($id)
    {
        $set=Settings::first();
        $currency=Currency::whereStatus(1)->first();
        $trans=Transfer::wherereceiver_id(Auth::user()->id)->first();
        $user=$data['user']=User::whereid($trans->receiver_id)->first();
        $trans->status=1;
        $trans->save();
        $user->balance=$user->balance+$trans->amount;
        $user->save();
        return redirect()->route('user.ownbank')->with('success', 'Transfer was successful');
    }
    public function localpreview()
    {
        $data['amount'] = Session::get('Amount');
        $data['email'] = Session::get('Acctemail');
        $data['title']='Transfer Preview';
        return view('user.transfer.preview', $data);
    } 
    public function ownbank()
    {
        $data['title'] = "Send Money";
        $data['transfer']=Transfer::whereSender_id(Auth::user()->id)->latest()->get();
        $data['received']=Transfer::wherereceiver_id(Auth::user()->id)->latest()->get();
        $data['sent']=Transfer::whereStatus(1)->whereSender_id(Auth::user()->id)->sum('amount');
        $data['pending']=Transfer::whereStatus(0)->wheresender_id(Auth::user()->id)->sum('amount');
        $data['rebursed']=Transfer::whereStatus(2)->wheresender_id(Auth::user()->id)->sum('amount');
        $data['total']=Transfer::wheresender_id(Auth::user()->id)->sum('amount');
        return view('user.transfer.index', $data);
    }
//End of Transfer

//Verification Code
    public function authCheck()
    {
        if (Auth()->user()->status == '0' && Auth()->user()->email_verify == '1' && Auth()->user()->sms_verify == '1') {
            return redirect()->route('user.dashboard');
        } else {
            $data['title'] = "Authorization";
            return view('user.profile.verify', $data);
        }
    }
    public function sendVcode(Request $request)
    {
        $user = User::find(Auth::user()->id);

        if (Carbon::parse($user->phone_time)->addMinutes(1) > Carbon::now()) {
            $time = Carbon::parse($user->phone_time)->addMinutes(1);
            $delay = $time->diffInSeconds(Carbon::now());
            $delay = gmdate('i:s', $delay);
            session()->flash('alert', 'You can resend Verification Code after ' . $delay . ' minutes');
        } else {
            $code = strtoupper(Str::random(6));
            $user->phone_time = Carbon::now();
            $user->sms_code = $code;
            $user->save();
            send_sms($user->phone, 'Your Verification Code is ' . $code);

            session()->flash('success', 'Verification Code Send successfully');
        }
        return back();
    }
    public function smsVerify(Request $request)
    {
        $user = User::find(Auth::user()->id);
        if ($user->sms_code == $request->sms_code) {
            $user->phone_verify = 1;
            $user->save();
            session()->flash('success', 'Your Profile has been verfied successfully');
            return redirect()->route('user.dashboard');
        } else {
            session()->flash('alert', 'Verification Code Did not match');
        }
        return back();
    }
    public function sendEmailVcode(Request $request)
    {
        $user = User::find(Auth::user()->id);

        if (Carbon::parse($user->email_time)->addMinutes(1) > Carbon::now()) {
            $time = Carbon::parse($user->email_time)->addMinutes(1);
            $delay = $time->diffInSeconds(Carbon::now());
            $delay = gmdate('i:s', $delay);
            session()->flash('alert', 'You can resend Verification Code after ' . $delay . ' minutes');
        } else {
            $code = strtoupper(Str::random(6));
            $user->email_time = Carbon::now();
            $user->verification_code = $code;
            $user->save();
            send_email($user->email, $user->username, 'Verificatin Code', 'Your Verification Code is ' . $code);
            session()->flash('success', 'Verification Code Send successfully');
        }
        return back();
    }
    public function postEmailVerify(Request $request)
    {

        $user = User::find(Auth::user()->id);
        if ($user->verification_code == $request->email_code) {
            $user->email_verify = 1;
            $user->save();
            session()->flash('success', 'Your Profile has been verfied successfully');
            return redirect()->route('user.dashboard');
        } else {
            session()->flash('alert', 'Verification Code Did not matched');
        }
        return back();
    } 
//End Verification Code

//Bank functions 
    public function bank()
    {
        $data['title']='Manage bank accounts';
        $data['bank']=Bank::whereUser_id(Auth::user()->id)->orderBy('id', 'DESC')->get();
        return view('user.bank.index', $data);
    }
    public function Destroybank($id)
    {
        $data = Bank::findOrFail($id);
        $res =  $data->delete();
        if ($res) {
            return back()->with('success', 'Bank account was Successfully deleted!');
        } else {
            return back()->with('alert', 'Problem With Deleting Request');
        }
    } 
    public function Updatebank(Request $request)
    {
        $bank=Bank::whereId($request->id)->first();
        $bank->name=$request->name;
        $bank->iban=$request->iban;
        $bank->swift=$request->swift;
        $bank->acct_no=$request->acct_no;
        $bank->acct_name=$request->acct_name;
        $bank->save();
        return back()->with('success', 'Bank details was successfully updated');
    }
    public function Createbank(Request $request)
    {
        $data['name']=$request->name;
        $data['iban']=$request->iban;
        $data['swift']=$request->swift;
        $data['acct_no']=$request->acct_no;
        $data['acct_name']=$request->acct_name;
        $data['user_id']=Auth::user()->id;
        Bank::create($data);
        return back()->with('success', 'Bank account was successfully added.');
    } 
//End of bank functions

//Settings
    public function submitPassword(Request $request)
    {
        $user = User::whereid(Auth::user()->id)->first();
        $user->password = Hash::make($request->password);
        $user->save();
        return back()->with('success', 'Password Changed Successfully.');
    }
    public function avatar(Request $request)
    {
        $user = User::findOrFail(Auth::user()->id);
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $filename = $user->username.time().'avatar.'.$image->extension();
            $location = 'asset/profile/' . $filename;
            if ($user->image != 'user-default.png') {
                $path = './asset/profile/';
                File::delete($path.$user->image);
            }
            Image::make($image)->save($location);
            $user->image=$filename;
            $user->save();
            return back()->with('success', 'Avatar Updated Successfully.');
        }else{
            return back()->with('success', 'An error occured, try again.');
        }
    }       
    public function kyc(Request $request)
    {
        $user = User::findOrFail(Auth::user()->id);
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $filename = $user->username.time().'kyc.'.$image->extension();
            $location = 'asset/profile/' . $filename;
            if ($user->image != 'user-default.png') {
                $path = './asset/profile/';
                $link = $path . $user->kyc_link;
                if (file_exists($link)) {
                    @unlink($link);
                }
            }
            Image::make($image)->save($location);
            $user->kyc_link=$filename;
            $user->save();
            return back()->with('success', 'Kyc document Upload was successful.');
        }else{
            return back()->with('success', 'An error occured, try again.');
        }
    }
    public function account(Request $request)
    {
        $user = User::findOrFail(Auth::user()->id);
        $user->first_name=$request->first_name;
        $user->last_name=$request->last_name;
        $user->address=$request->address;
        $user->save();
        if($user->phone!=$request->phone){
            $user->phone_verify=0;
            $user->phone=$request->phone;
            $user->save();
        }        
        if($user->email!=$request->email){
            $check=User::whereEmail($request->email)->get();
            if(count($check)<1){
                $user->email_verify=0;
                $user->email=$request->email;
                $user->save();
            }else{
                return back()->with('alert', 'Email already in use.');
            }
        }
        return back()->with('success', 'Profile Updated Successfully.');
    }
    public function Destroyuser($id)
    {
        $user = User::whereId($id)->delete();
        $transfer = Transfer::whereUser_id($id)->delete();
        $bank_transfer = Banktransfer::whereUser_id($id)->delete();
        $deposit = Deposits::whereUser_id($id)->delete();
        $ticket = Ticket::whereUser_id($id)->delete();
        $withdraw = Withdraw::whereUser_id($id)->delete();
        $profit = Profits::whereUser_id($id)->delete();
        $bank = Bank::whereUser_id($id)->delete();
        Auth::guard()->logout();
        session()->forget('fakey');
        return redirect()->route('login')->with('success', 'Account was successfully deleted');
    }  
    public function profile()
    {
        $data['title'] = "Profile";
        $g=new \Sonata\GoogleAuthenticator\GoogleAuthenticator();
        $secret=$g->generateSecret();
        $set=Settings::first();
        $user = User::find(Auth::user()->id);
        $site=$set->site_name;
        $data['secret']=$secret;
        $data['image']=\Sonata\GoogleAuthenticator\GoogleQrUrl::generate($user->email, $secret, $site);
        return view('user.profile.index', $data);
    }    
    public function submit2fa(Request $request)
    {
        $user = User::findOrFail(Auth::user()->id);
        $g=new \Sonata\GoogleAuthenticator\GoogleAuthenticator();
        $secret=$request->vv;
        if ($request->type==0) {
            $check=$g->checkcode($secret, $request->code, 3);
            if($check){
                $user->fa_status = 0;
                $user->googlefa_secret = null;
                $user->save();
                return back()->with('success', '2fa disabled.');
            }else{
                return back()->with('alert', 'Invalid code.');
            }
        }else{
            $check=$g->checkcode($secret, $request->code, 3);
            if($check){
                $user->fa_status = 1;
                $user->googlefa_secret = $request->vv;
                $user->save();
                return back()->with('success', '2fa enabled.');
            }else{
                return back()->with('alert', 'Invalid code.');
            }
        }
    }
//End of Settings

//Referral
    public function referral()
    {
        $data['title']='Referral';
        $data['referral']=Referral::whereRef_id(Auth::user()->id)->get();
        $data['earning']=Earning::whereReferral(Auth::user()->id)->get();
        return view('user.profile.referral', $data);
    }
//End of Referral

    public function dashboard()
    {
        $data['title']='Dashboard';
        $data['plan']=Plans::whereStatus(1)->orderBy('amount', 'DESC')->limit(1)->first();
        $data['profit']=Profits::whereUser_id(Auth::user()->id)->orderBy('id', 'DESC')->limit(5)->get();
        $data['received']=Withdraw::whereStatus(1)->whereuser_id(Auth::user()->id)->sum('amount');
        $data['pending']=Withdraw::whereStatus(0)->whereuser_id(Auth::user()->id)->sum('amount');
        $data['total']=Withdraw::whereuser_id(Auth::user()->id)->sum('amount');
        return view('user.index', $data);
    }   
    public function plans()
    {
        $data['title']='Investment';
        $data['plan']=Plans::whereStatus(1)->orderBy('min_deposit', 'DESC')->get();
        $data['profit']=Profits::whereUser_id(Auth::user()->id)->orderBy('id', 'DESC')->get();
        $data['datetime']=Carbon::now();
        return view('user.plans', $data);
    }  
    
    public function calculate(Request $request)
    {
        $plan=Plans::find($request->plan_id);
        $duration=$plan->duration.' '.$plan->period;
        $currency=Currency::whereStatus(1)->first();
        $profit=$request->amount*($plan->percent/100)*castrotime($duration);
        if($request->amount>$plan->min_deposit || $request->amount==$plan->min_deposit){
            if($request->amount<$plan->amount  || $request->amount==$plan->amount){
                return back()->with('success', $currency->symbol.$profit);  
            }else{
                return back()->with('alert', 'value is greater than maximum deposit');  
            }
        }else{
            return back()->with('alert', 'value is less than minimum deposit');  
        }
    }
    
    public function buy(Request $request)
    {
        $plan=$data['plan']=Plans::Where('id',$request->plan)->first();
        $user=User::find(Auth::user()->id);
        $set=Settings::find(1);
        if($user->balance>$request->amount || $user->balance==$request->amount){
            if($request->amount>$plan->min_deposit || $request->amount==$plan->min_deposit){
                if($request->amount<$plan->amount  || $request->amount==$plan->amount){
                    $sav['user_id']=Auth::user()->id;
                    $sav['plan_id']=$request->plan;
                    $sav['amount']=$request->amount;
                    $sav['profit']=0;
                    $sav['status']=1;
                    $sav['end_date']=0;
                    $sav['date']=Carbon::now();
                    $sav['trx']=str_random(16);
                    Profits::create($sav);
                    $a=$user->balance-$request->amount;
                    $user->balance=$a;
                    $user->save();
                    if ($plan->bonus!=null){
                        if ($set->referral==1){
                            $kex=Referral::whereUser_id(Auth::user()->id)->get();
                            if(count($kex)>0){
                                $ref_amount=($request->amount*$plan->ref_percent)/100;
                                $key=Referral::whereUser_id(Auth::user()->id)->first();
                                $user_update=User::whereId($key->ref_id)->first();
                                $user_update->ref_bonus=$user_update->ref_bonus+$ref_amount;
                                $user_update->save();
                                $ref['user_id']=Auth::user()->id;
                                $ref['referral']=$key->ref_id;
                                $ref['plan_id']=$request->plan;
                                $ref['ref_id']=$key->ref_id;
                                $ref['amount']=str_random(16);
                                Earning::create($ref);
                            }
                        }
                    }
                    return back()->with('success', 'Plan was successfully purchased, scroll down to watch your daily earnings');  
                }else{
                    return back()->with('alert', 'value is greater than maximum deposit');  
                }
            }else{
                return back()->with('alert', 'value is less than minimum deposit');  
            }
        }else{
            return back()->with('alert', 'Insufficient Funds, please fund your account');
        }
    } 

    public function logout()
    {
        Auth::guard()->logout();
        session()->flash('message', 'Just Logged Out!');
        return redirect('/login');
    }
}
