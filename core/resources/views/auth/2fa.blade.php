@extends('layout')
@section('css')

@stop
@section('content')
<section id="header" class="backg backg-one bg-banner-gradient">
  <div class="container">
      <div class="backg-content-wrap">
          <div class="row align-items-center">
              <div class="col-lg-6 z100">
                  <div class="backg-content">
                      <span class="discount wow soneFadeUp" data-wosw-delay="0.3s">{{__('Two Factor Authentication,')}}</span>
                      <h1 class="backg-title wow soneFadeUp" data-wow-delay="0.5s">
                      {{__('Verify your')}} {{$set->site_name}} {{__('Account')}}
                      </h1>     
                      <span class="text-medium">{{__('Trouble signing in? ')}}<a href="mailto:{{$set->email}}">{{__('contact support')}}</a></span>             
                  </div>
              </div>
              <div class="col-lg-6">
                  <div class="wow soneFadeLeft">
                    <div class="pt-100"></div>
                    <form action="{{route('submitfa')}}" method="post" class="contact-form" data-saasone="contact-froms">
                        @csrf
                      <input type="text" name="code" placeholder="Code" required>
                      <div class="text-left">
                        <a href="{{route('user.password.request')}}"><span class="text-medium">{{__('Forgot password?')}}</span></a>
                      </div>                              
                      <div class="text-right">
                        <button type="submit" class="sone-btn">{{__('Sign In')}}</button>
                      </div>
                    </form>
                  </div>
              </div>
          </div>
      </div>
  </div>
</section>
@stop