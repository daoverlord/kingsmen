@extends('layout')
@section('css')

@stop
@section('content')
<section id="header" class="backg backg-one bg-banner-gradient">
  <div class="container">
      <div class="backg-content-wrap">
          <div class="row align-items-center">
              <div class="col-lg-6 z100">
                  <div class="backg-content">
                      <span class="discount wow soneFadeUp" data-wosw-delay="0.3s">{{__('Create an account')}}</span>
                      <h1 class="backg-title wow soneFadeUp" data-wow-delay="0.5s">
                      {{__('Lets get to know you')}}
                      </h1>     
                      <span class="text-medium">{{__('Need help?')}} <a href="mailto:{{$set->email}}">{{__('contact support')}}</a></span>             

                  </div>
              </div>
              <div class="col-lg-6">
                  <div class="wow soneFadeLeft">
                    <div class="pt-100"></div>
                    @if($set->registration==1)
                      <form action="{{route('submitregister')}}" method="post" class="contact-form" data-saasone="contact-froms">
                          @csrf
                        <div class="row">
                          <div class="col-lg-6">
                            <input placeholder="First Name" type="text" name="first_name" required>
                          </div>                        
                          <div class="col-lg-6">
                            <input placeholder="Last Name" type="text" name="last_name" required>
                          </div>
                        </div>            
                        <input placeholder="Username" type="text" name="username" required>
                        @if ($errors->has('username'))
                          <span class="error form-error-msg ">
                              {{ $errors->first('username') }}
                          </span>
                        @endif
                        <input type="email" name="email" placeholder="Email Address" required>
                        @if ($errors->has('email'))
                            <span class="error form-error-msg ">
                                {{ $errors->first('email') }}
                            </span>
                        @endif
                        <input type="password" name="password" placeholder="Password" required>
                        @if ($errors->has('password'))
                            <span class="error form-error-msg ">
                                {{ $errors->first('password') }}
                            </span>
                        @endif
                        <div class="text-left">
                          <a href="{{route('login')}}"><span class="text-small">{{__('Got an account?')}}</span></a>
                        </div>                              
                        <div class="text-right">
                          <button type="submit" class="sone-btn">{{__('Continue')}}</button>
                        </div>
                      </form>
                    @else
                      <div class="text-dark text-center mt-2 mb-3"><strong>{{__('We are not currenctly accepting new users')}}</strong></div>
                    @endif
                  </div>
                  <!-- /.promo-mockup -->
              </div>
              <!-- /.col-lg-6 -->
          </div>
          <!-- /.row -->
      </div>
      <!-- /.backg-content-wrap -->
  </div>
  <!-- /.container -->
</section>
@stop
