@extends('layout')
@section('css')

@stop
@section('content')
<section id="header" class="backg backg-one bg-banner-gradient">
  <div class="container">
    <div class="backg-content-wrap">
      <div class="row align-items-center">
        <div class="col-lg-6 z100">
          <div class="backg-content">
            <span class="discount wow soneFadeUp" data-wosw-delay="0.3s">{{__('Still cant Remember,')}}</span>
            <h1 class="backg-title wow soneFadeUp" data-wow-delay="0.5s">
            {{__('Reset password')}}
            </h1>                  
            <span class="text-medium">{{__('Still got trouble?')}} <a href="mailto:{{$set->email}}">{{__('contact support')}}</a></span>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="wow soneFadeLeft">
            <div class="pt-100"></div>
            <form action="{{ route('user.password.email') }}" method="post" class="contact-form" data-saasone="contact-froms">
              @csrf
              <input type="email" name="email" placeholder="Email" required>                        
              <div class="text-right">
                <button type="submit" class="sone-btn">{{__('Reset')}}</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@stop