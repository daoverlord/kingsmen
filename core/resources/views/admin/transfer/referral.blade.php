@extends('master')

@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h6 class="card-title font-weight-semibold">{{__('Earnings')}}</h6>
                    </div>
                    <div class="">
                        <table class="table datatable-responsive">
                            <thead>
                                <tr>
                                    <th>{{__('S/N')}}</th>
                                    <th>{{__('Amount')}}</th>
                                    <th>{{__('Username')}}</th>
                                    <th>{{__('From')}}</th>
                                    <th>{{__('Plan')}}</th>
                                    <th>{{__('Created')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($earning as $k=>$val)
                                <tr>
                                    <td>{{++$k}}.</td>
                                    <td>{{$currency->symbol.number_format($val->amount)}}</td>
                                    <td>{{$val->user['first_name']}} {{$val->user['last_name']}}</td>
                                    <td><a href="{{url('admin/manage-user')}}/{{$val->user['id']}}">{{$val->shared['username']}}</a></td>
                                    <td>{{$val->plan['name']}}</td>
                                    <td>{{date("Y/m/d", strtotime($val->created_at))}}</td>  
                                @endforeach               
                            </tbody>                    
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop