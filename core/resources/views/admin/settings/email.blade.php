@extends('master')

@section('content')
<div class="content"> 
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h6 class="card-title font-weight-semibold">{{__('Email Template')}}</h6>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>{{__('S/N')}}</th>
                                <th>{{__('CODE')}}</th>
                                <th>{{__('DESCRIPTION')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td> {{__('1')}} </td>
                            <td> &#123;&#123;message&#125;&#125;</td>
                            <td> {{__('Details Text From the Script')}}</td>
                        </tr>
                        <tr>
                            <td> {{__('2')}} </td>
                            <td> &#123;&#123;name&#125;&#125;</td>
                            <td> {{__('Users Name. Will be Pulled From Database')}}</td>
                        </tr>
                        </tbody>                    
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h6 class="card-title font-weight-semibold">{{__('Congifure template')}}</h6>
                </div>
                <div class="card-body">
                    <form action="{{route('admin.email.update')}}" method="post">
                    @csrf
                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">{{__('Email sent from')}}</label>
                            <div class="col-lg-10">
                                <input type="text" name="sender" maxlength="200" value="{{$val->esender}}" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">{{__('Email message')}}</label>
                            <div class="col-lg-10">
                                <textarea type="text" name="message" rows="4" class="form-control tinymce">{{$val->emessage}}</textarea>
                            </div>
                        </div>          
                    <div class="text-right">
                        <button type="submit" class="btn btn-success btn-sm">{{__('Save Changes')}}</button>
                    </div>
                </form>
            </div>
        </div>    
    </div>
</div>
@stop