@extends('master')

@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h6 class="card-title font-weight-semibold">{{__('Users')}}</h6>
                    </div>
                    <div class="">
                        <table class="table datatable-responsive">
                            <thead>
                                <tr>
                                    <th>{{__('S/N')}}</th>
                                    <th>{{__('Full Name')}}</th>
                                    <th>{{__('Username')}}</th>
                                    <th>{{__('Email')}}</th>                                                                      
                                    <th>{{__('Status')}}</th>
                                    <th>{{__('Balance')}}</th>
                                    <th>{{__('Profit')}}</th>
                                    <th>{{__('Referral Bonus')}}</th>
                                    <th>{{__('Created')}}</th>
                                    <th>{{__('Updated')}}</th>
                                    <th class="text-center">{{__('Action')}}</th>    
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $k=>$val)
                                <tr>
                                    <td>{{++$k}}.</td>
                                    <td>{{$val->first_name}} {{$val->last_name}}</td>
                                    <td>{{$val->username}}</td>
                                    <td>{{$val->email}}</td>
                                    <td>
                                        @if($val->status==0)
                                            <span class="badge badge-info">{{__('Active')}}</span>
                                        @elseif($val->status==1)
                                            <span class="badge badge-danger">{{__('Blocked')}}</span> 
                                        @endif
                                    </td>   
                                    <td>{{$currency->symbol.number_format($val->balance)}}</td> 
                                    <td>{{$currency->symbol.number_format($val->profit)}}</td> 
                                    <td>{{$currency->symbol.number_format($val->ref_bonus)}}</td> 
                                    <td>{{date("Y/m/d h:i:A", strtotime($val->created_at))}}</td>  
                                    <td>{{date("Y/m/d h:i:A", strtotime($val->updated_at))}}</td>
                                    <td class="text-center">
                                        <div class="list-icons">
                                            <div class="dropdown">
                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a class='dropdown-item' href="{{url('/')}}/admin/manage-user/{{$val->id}}"><i class="icon-cogs spinner mr-2"></i>{{__('Manage account')}}</a>
                                                    @if($val->status==0)
                                                        <a class='dropdown-item' href="{{url('/')}}/admin/block-user/{{$val->id}}"><i class="icon-eye-blocked2 mr-2"></i>{{__('Block')}}</a>
                                                    @else
                                                        <a class='dropdown-item' href="{{url('/')}}/admin/unblock-user/{{$val->id}}"><i class="icon-eye mr-2"></i>{{__('Unblock')}}</a>
                                                    @endif
                                                    <a class='dropdown-item' href="{{url('/')}}/admin/email/{{$val->email}}/{{$val->business_name}}"><i class="icon-envelope mr-2"></i>{{__('Send email')}}</a>    
                                                    <a data-toggle="modal" data-target="#{{$val->id}}delete" class="dropdown-item"><i class="icon-bin2 mr-2"></i>{{__('Delete account')}}</a>
                                                </div>
                                            </div>
                                        </div>
                                    </td>                   
                                </tr>
                                <div id="{{$val->id}}delete" class="modal fade" tabindex="-1">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">   
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>
                                            <div class="modal-body">
                                                <h6 class="font-weight-semibold">{{__('Are you sure you want to delete this?')}}</h6>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-link" data-dismiss="modal">{{__('Close')}}</button>
                                                <a  href="{{url('/')}}/admin/user/delete/{{$val->id}}" class="btn bg-danger">{{__('Proceed')}}</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach               
                            </tbody>                    
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop