@extends('master')

@section('content')
<div class="content">
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h6 class="card-title font-weight-semibold">{{__('Update account information')}}</h6>
                </div>
                <div class="card-body">
                        <form action="{{url('admin/profile-update')}}" method="post">
                        @csrf
                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">{{__('Username:')}}</label>
                            <div class="col-lg-10">
                                <input type=""hidden value="{{$client->id}}" name="id">
                                <input type="text" name="username" class="form-control" value="{{$client->username}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">{{__('First Name:')}}</label>
                            <div class="col-lg-4">
                                <input type="text" name="first_name" class="form-control" value="{{$client->first_name}}">
                            </div>                            
                            <label class="col-form-label col-lg-2">{{__('Last Name:')}}</label>
                            <div class="col-lg-4">
                                <input type="text" name="last_name" class="form-control" value="{{$client->last_name}}">
                            </div>
                        </div>  
                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">{{__('Email:')}}</label>
                            <div class="col-lg-10">
                                <input type="email" name="email" class="form-control" readonly value="{{$client->email}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">{{__('Mobile:')}}</label>
                            <div class="col-lg-10">
                                <input type="text" name="mobile" class="form-control" value="{{$client->phone}}">
                            </div>
                        </div>                                         
                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">{{__('Address:')}}</label>
                            <div class="col-lg-10">
                                <input type="text" name="address" class="form-control" value="{{$client->address}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">{{__('Balance:')}}</label>
                            <div class="col-lg-10">
                                <div class="input-group">
                                    <span class="input-group-prepend">
                                        <span class="input-group-text">{{$currency->symbol}}</span>
                                    </span>
                                    <input type="number" name="balance"step="any" max-length="10" value="{{convertFloat($client->balance)}}" class="form-control">
                                </div>
                            </div>
                        </div> 
                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">{{__('Status')}}<span class="text-danger">*</span></label>
                            <div class="col-lg-10">
                                <div class="form-check form-check-inline form-check-switchery">
                                    <label class="form-check-label">
                                        @if($client->email_verify==1)
                                            <input type="checkbox" name="email_verify" class="form-check-input-switchery" value="1" checked>
                                        @else
                                            <input type="checkbox" name="email_verify" class="form-check-input-switchery" value="1">
                                        @endif  
                                        {{__('Email verification ')}} 
                                    </label>
                                </div>                                
                                <div class="form-check form-check-inline form-check-switchery">
                                    <label class="form-check-label">
                                        @if($client->phone_verify==1)
                                            <input type="checkbox" name="phone_verify" class="form-check-input-switchery" value="1" checked>
                                        @else
                                            <input type="checkbox" name="phone_verify" class="form-check-input-switchery" value="1">
                                        @endif  
                                        {{__('Phone verification  ')}}
                                    </label>
                                </div> 
                                <div class="form-check form-check-inline form-check-switchery">
                                    <label class="form-check-label">
                                        @if($client->fa_status==1)
                                            <input type="checkbox" name="fa_status" class="form-check-input-switchery" value="1" checked>
                                        @else
                                            <input type="checkbox" name="fa_status" class="form-check-input-switchery" value="1">
                                        @endif  
                                        {{__('2fa')}}  
                                    </label>
                                </div>                                
                            </div>
                        </div>                
                        <div class="text-right">
                            <button type="submit" class="btn btn-success btn-sm">{{__('Save')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body text-center">
                    <div class="card-img-actions d-inline-block mb-3">
                        <img class="img-fluid rounded-circle" src="{{url('/')}}/asset/profile/{{$client->image}}" width="120" height="120" alt="">
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="d-sm-flex align-item-sm-center flex-sm-nowrap">
                        <div>
                            <ul class="list list-unstyled mb-0">
                                <li>{{__('Joined:')}} <span class="font-weight-semibold">{{date("Y/m/d h:i:A", strtotime($client->created_at))}}</span></li>
                                <li>{{__('Last Login:')}} <span class="font-weight-semibold">{{date("Y/m/d h:i:A", strtotime($client->last_login))}}</span></li>
                                <li>{{__('Last Updated:')}} <span class="font-weight-semibold">{{date("Y/m/d h:i:A", strtotime($client->updated_at))}}</span></li>
                                <li>{{__('IP Address:')}} <span class="font-weight-semibold">{{$client->ip_address}}</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>  
            @if($set->kyc==1)
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h6 class="card-title font-weight-semibold">{{__('Kyc verification')}}</h6>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                            <th>#</th>
                            <th class="text-center">{{__('Status')}}</th>
                            <th class="text-center">{{__('Action')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center">
                                    @if($client->kyc_status==0)
                                    {{__('Unverified')}}
                                    @else
                                    {{__('Verified')}}
                                    @endif
                                </td>
                                <td class="text-center">
                                    @if(!empty($client->kyc_link))
                                        <a href="{{url('/')}}/asset/profile/{{$client->kyc_link}}">{{__('View')}}</a>
                                    @else
                                    {{__('No file')}}
                                    @endif
                                </td>
                                <td class="text-center">
                                @if($client->kyc_status!=1)
                                    @if(!empty($client->kyc_link)) 
                                    <div class="list-icons">
                                        <div class="dropdown">
                                            <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                <i class="icon-menu9"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class='dropdown-item' href ="{{url('/')}}/admin/approve-kyc/{{$client->id}}"><i class="icon-eye mr-2"></i>{{__('Approve')}}</a>
                                                <a class='dropdown-item' href ="{{url('/')}}/admin/reject-kyc/{{$client->id}}"><i class="icon-eye-blocked2 mr-2"></i>{{__('Reject')}}</a>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                @endif
                                </td>             
                            </tr> 
                        </tbody>
                    </table>
                </div>
            </div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h6 class="card-title font-weight-semibold">{{__('Deposit Logs')}}</h6>
                </div>
                <div class="">
                    <table class="table datatable-responsive">
                        <thead>
                            <tr>
                                <th>{{__('S/N')}}</th>
                                <th>{{__('Amount')}}</th>                                                                                                                                       
                                <th>{{__('Method')}}</th>
                                <th>{{__('Ref')}}</th>
                                <th>{{__('Charge')}}</th>
                                <th>{{__('Status')}}</th>
                                <th>{{__('Created')}}</th>
                                <th>{{__('Updated')}}</th>
                                <th class="text-center">{{__('Action')}}</th>    
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($deposit as $k=>$val)
                            <tr>
                                <td>{{++$k}}.</td>
                                <td>{{$currency->symbol.number_format($val->amount)}}</td>
                                <td>{{$val->gateway['name']}}</td>
                                <td>{{$val->trx}}</td> 
                                <td>{{$currency->symbol.number_format($val->charge)}}</td> 
                                <td>
                                    @if($val->status==0)
                                        <span class="badge badge-danger">{{__('Pending')}}</span>
                                    @elseif($val->status==1)
                                        <span class="badge badge-success">{{__('Approved')}}</span>  
                                    @elseif($val->status==2)
                                        <span class="badge badge-info">{{__('Declined')}}</span> 
                                    @endif
                                </td>  
                                <td>{{date("Y/m/d h:i:A", strtotime($val->created_at))}}</td>
                                <td>{{date("Y/m/d h:i:A", strtotime($val->updated_at))}}</td>
                                <td class="text-center">
                                    <div class="list-icons">

                                        <div class="dropdown">
                                            <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                <i class="icon-menu9"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                            @if($val->status==0)
                                                <a class='dropdown-item' href="{{url('/')}}/admin/approvedeposit/{{$val->id}}"><i class="icon-thumbs-up3 mr-2"></i>{{__('Approve')}}</a>
                                                <a class='dropdown-item' href="{{url('/')}}/admin/declinedeposit/{{$val->id}}"><i class="icon-thumbs-down3 mr-2"></i>{{__('Decline')}}</a>
                                            @endif
                                                <a data-toggle="modal" data-target="#{{$val->id}}delete" class="dropdown-item"><i class="icon-bin2 mr-2"></i>{{__('Delete')}}</a>
                                            </div>
                                        </div>
                                    </div>
                                </td>                   
                            </tr>
                            <div id="{{$val->id}}delete" class="modal fade" tabindex="-1">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">   
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <div class="modal-body">
                                            <h6 class="font-weight-semibold">{{__('Are you sure you want to delete this?')}}</h6>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-link" data-dismiss="modal">{{__('Close')}}</button>
                                            <a  href="{{url('/')}}/admin/deposit/delete/{{$val->id}}" class="btn bg-danger">{{__('Proceed')}}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach               
                        </tbody>                    
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h6 class="card-title font-weight-semibold">{{__('Withdraw logs')}}</h6>
                </div>
                <div class="">
                    <table class="table datatable-responsive">
                        <thead>
                            <tr>
                                <th>{{__('S/N')}}</th>
                                <th>{{__('Amount')}}</th>                                                                     
                                <th>{{__('Details')}}</th>
                                <th>{{__('Method')}}</th>
                                <th>{{__('Status')}}</th>
                                <th>{{__('Type')}}</th>
                                <th>{{__('Created')}}</th>
                                <th>{{__('Updated')}}</th>
                                <th class="text-center">{{__('Action')}}</th>    
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($withdraw as $k=>$val)
                            <tr>
                                <td>{{++$k}}.</td>
                                <td>{{number_format($val->amount).$currency->name}}</td>
                                <td>{{$val->details}}</td>
                                <td>{{$val->wallet->method}}</td>
                                <td>
                                    @if($val->status==0)
                                        <span class="badge badge-danger">{{__('Unpaid')}}</span>
                                    @elseif($val->status==1)
                                        <span class="badge badge-success">{{__('Paid')}}</span> 
                                    @elseif($val->status==2)
                                        <span class="badge badge-info">{{__('Declined')}}</span>
                                    @endif
                                </td> 
                                <td>          
                                    @if($val->type==1)
                                        <span class="badge badge-info">{{__('Trading profit')}}</span>
                                    @elseif($val->type==2)
                                        <span class="badge badge-info">{{__('Account balance')}}</span>                  
                                    @elseif($val->type==3)
                                        <span class="badge badge-info">{{__('Referral bonus')}}</span>
                                    @endif
                                </td> 
                                <td>{{date("Y/m/d h:i:A", strtotime($val->created_at))}}</td>
                                <td>{{date("Y/m/d h:i:A", strtotime($val->updated_at))}}</td>
                                <td class="text-center">
                                    <div class="list-icons">
                                        <div class="dropdown">
                                            <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                <i class="icon-menu9"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                            @if($val->status==0)
                                                <a class='dropdown-item' href="{{url('/')}}/admin/approvewithdraw/{{$val->id}}"><i class="icon-thumbs-up3 mr-2"></i>{{__('Approve')}}</a>
                                                <a class='dropdown-item' href="{{url('/')}}/admin/declinewithdraw/{{$val->id}}"><i class="icon-thumbs-down3 mr-2"></i>{{__('Decline')}}</a>
                                            @endif
                                                <a data-toggle="modal" data-target="#{{$val->id}}delete" class="dropdown-item"><i class="icon-bin2 mr-2"></i>{{__('Delete')}}</a>
                                            </div>
                                        </div>
                                    </div>
                                </td>                   
                            </tr>
                            <div id="{{$val->id}}delete" class="modal fade" tabindex="-1">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">   
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <div class="modal-body">
                                            <h6 class="font-weight-semibold">{{__('Are you sure you want to delete this?')}}</h6>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-link" data-dismiss="modal">{{__('Close')}}</button>
                                            <a  href="{{url('/')}}/admin/withdraw/delete/{{$val->id}}" class="btn bg-danger">{{__('Proceed')}}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach               
                        </tbody>                    
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h6 class="card-title font-weight-semibold">{{__('Investment')}}</h6>
                </div>
                <div class="">
                    <table class="table datatable-show-all">
                        <thead>
                            <tr>
                                <th>{{__('S/N')}}</th>
                                <th>{{__('Ref')}}</th>
                                <th>{{__('Amount')}}</th>                                                                       
                                <th>{{__('Plan')}}</th>
                                <th>{{__('Daily percent')}}</th>
                                <th>{{__('Duration')}}</th>
                                <th>{{__('Profit')}}</th>
                                <th>{{__('Created')}}</th>
                                <th>{{__('Updated')}}</th>
                                <th class="text-center">{{__('Action')}}</th>    
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($profit as $k=>$val)
                            <tr>
                                <td>{{++$k}}.</td>
                                <td>{{$val->trx}}</td>
                                <td>{{substr($val->amount,0,9).$currency->name}}</td>
                                <td>{{$val->plan->name}}</td>
                                <td>{{$val->plan->percent}}%</td>
                                <td>{{$val->plan->duration.$val->plan->period}}(s)</td>
                                <td>{{substr($val->profit,0,9).$currency->name}}</td>
                                <td>{{date("Y/m/d", strtotime($val->created_at))}}</td>  
                                <td>{{date("Y/m/d h:i:A", strtotime($val->updated_at))}}</td>
                                <td class="text-center">
                                    <div class="list-icons">
                                        <div class="dropdown">
                                            <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                <i class="icon-menu9"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a data-toggle="modal" data-target="#{{$val->id}}delete" class="dropdown-item"><i class="icon-bin2 mr-2"></i>{{__('Delete')}}</a>
                                            </div>
                                        </div>
                                    </div>
                                </td>                   
                            </tr>
                            <div id="{{$val->id}}delete" class="modal fade" tabindex="-1">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">   
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <div class="modal-body">
                                            <h6 class="font-weight-semibold">{{__('Are you sure you want to delete this?')}}</h6>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-link" data-dismiss="modal">{{__('Close')}}</button>
                                            <a  href="{{url('/')}}/admin/py/delete/{{$val->id}}" class="btn bg-danger">{{__('Proceed')}}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach               
                        </tbody>                    
                    </table>
                </div>
            </div>
        </div>
    </div>   
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h6 class="card-title font-weight-semibold">{{__('Ticket')}}</h6>
                </div>
                <div class="">
                    <table class="table datatable-show-all">
                        <thead>
                            <tr>
                                <th>{{__('S/N')}}</th>
                                <th>{{__('Priority')}}</th>
                                <th>{{__('Ticket ID')}}</th>                                                                      
                                <th>{{__('Status')}}</th>
                                <th>{{__('Subject')}}</th>
                                <th>{{__('Created')}}</th>
                                <th>{{__('Updated')}}</th>
                                <th class="text-center">{{__('Action')}}</th>    
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($ticket as $k=>$val)
                            <tr>
                                <td>{{++$k}}.</td>
                                <td>{{$val->priority}}</td>
                                <td>{{$val->ticket_id}}</td>
                                <td>
                                    @if($val->status==0)
                                        <span class="badge badge-info">{{__('Open')}}</span>
                                    @elseif($val->status==1)
                                        <span class="badge badge-danger">{{__('Closed')}}</span>                                        
                                    @elseif($val->status==2)
                                        <span class="badge badge-success">{{__('Resolved')}}</span> 
                                    @endif
                                </td>   
                                <td>{{$val->subject}}</td> 
                                <td>{{date("Y/m/d", strtotime($val->date))}}</td>  
                                <td>{{date("Y/m/d h:i:A", strtotime($val->updated_at))}}</td>
                                <td class="text-center">
                                    <div class="list-icons">
                                        <div class="dropdown">
                                            <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                <i class="icon-menu9"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class='dropdown-item' href="{{url('/')}}/admin/manage-ticket/{{$val->id}}"><i class="icon-bubbles5 mr-2"></i>{{__('Manage ticket')}}</a>
                                                @if($val->status==0)
                                                    <a class='dropdown-item' href="{{url('/')}}/admin/close-ticket/{{$val->id}}"><i class="icon-eye-blocked2 mr-2"></i>{{__('Close ticket')}}</a>
                                                @endif    
                                                <a data-toggle="modal" data-target="#{{$val->id}}delete" class="dropdown-item"><i class="icon-bin2 mr-2"></i>{{__('Delete')}}</a>
                                            </div>
                                        </div>
                                    </div>
                                </td>                   
                            </tr>
                            <div id="{{$val->id}}delete" class="modal fade" tabindex="-1">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">   
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <div class="modal-body">
                                            <h6 class="font-weight-semibold">{{__('Are you sure you want to delete this?')}}</h6>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-link" data-dismiss="modal">{{__('Close')}}</button>
                                            <a  href="{{url('/')}}/admin/ticket/delete/{{$val->id}}" class="btn bg-danger">{{__('Proceed')}}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach               
                        </tbody>                    
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h6 class="card-title font-weight-semibold">{{__('Transfer logs')}}</h6>
                </div>
                <div class="">
                    <table class="table datatable-responsive">
                        <thead>
                            <tr>
                                <th>{{__('S/N')}}</th>
                                <th>{{__('Ref')}}</th>
                                <th>{{__('Sender')}}</th>
                                <th>{{__('Receiver')}}</th>
                                <th>{{__('Amount')}}</th>                                                                       
                                <th>{{__('Charge')}}</th>                                                                       
                                <th>{{__('Status')}}</th>
                                <th>{{__('Created')}}</th>
                                <th>{{__('Updated')}}</th>
                                <th class="text-center">{{__('Action')}}</th>    
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($transfer as $k=>$val)
                            <tr>
                                <td>{{++$k}}.</td>
                                <td>{{$val->ref_id}}</td>
                                <td>{{$val->sender['email']}}</td>
                                <td>
                                @if($val->receiver_id==null)  
                                    {{$val->temp}}
                                @else
                                    {{$val->receiver['email']}}
                                @endif
                                </td>
                                <td>{{$currency->symbol.number_format($val->amount)}}</td>
                                <td>{{$currency->symbol.number_format($val->charge)}}</td>
                                <td>
                                    @if($val->status==0)
                                        <span class="badge badge-danger">{{__('Pending')}}</span>
                                    @elseif($val->status==1)
                                        <span class="badge badge-success">{{__('Successful')}}</span>                                        
                                    @elseif($val->status==2)
                                        <span class="badge badge-info">{{__('Returned')}}</span> 
                                    @endif
                                </td>     
                                <td>{{date("Y/m/d h:i:A", strtotime($val->created_at))}}</td>  
                                <td>{{date("Y/m/d h:i:A", strtotime($val->updated_at))}}</td>
                                <td class="text-center">
                                    <div class="list-icons">
                                        <div class="dropdown">
                                            <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                <i class="icon-menu9"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a data-toggle="modal" data-target="#{{$val->id}}delete" class="dropdown-item"><i class="icon-bin2 mr-2"></i>{{__('Delete')}}</a>
                                            </div>
                                        </div>
                                    </div>
                                </td>                   
                            </tr>
                            <div id="{{$val->id}}delete" class="modal fade" tabindex="-1">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">   
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <div class="modal-body">
                                            <h6 class="font-weight-semibold">{{__('Are you sure you want to delete this?')}}</h6>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-link" data-dismiss="modal">{{__('Close')}}</button>
                                            <a  href="{{url('/')}}/admin/own-bank/delete/{{$val->id}}" class="btn bg-danger">{{__('Proceed')}}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach               
                        </tbody>                    
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h6 class="card-title font-weight-semibold">{{__('Earnings')}}</h6>
                </div>
                <div class="">
                    <table class="table datatable-responsive">
                        <thead>
                            <tr>
                                <th>{{__('S/N')}}</th>
                                <th>{{__('Amount')}}</th>
                                <th>{{__('Username')}}</th>
                                <th>{{__('From')}}</th>
                                <th>{{__('Plan')}}</th>
                                <th>{{__('Created')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($earning as $k=>$val)
                            <tr>
                                <td>{{++$k}}.</td>
                                <td>{{$currency->symbol.number_format($val->amount)}}</td>
                                <td>{{$val->user['first_name']}} {{$val->user['last_name']}}</td>
                                <td><a href="{{url('admin/manage-user')}}/{{$val->user['id']}}">{{$val->shared['username']}}</a></td>
                                <td>{{$val->plan['name']}}</td>
                                <td>{{date("Y/m/d", strtotime($val->created_at))}}</td>  
                            @endforeach               
                        </tbody>                    
                    </table>
                </div>
            </div>
        </div>
    </div>   
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h6 class="card-title font-weight-semibold">{{__('Referrals')}}</h6>
                </div>
                <div class="">
                    <table class="table datatable-show-all">
                        <thead>
                            <tr>
                                <th>{{__('S/N')}}</th>
                                <th>{{__('Name')}}</th>
                                <th>{{__('Username')}}</th>
                                <th>{{__('Created')}}</th>
                                <th>{{__('Updated')}}</th>  
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($referral as $k=>$val)
                            <tr>
                                <td>{{++$k}}.</td>
                                <td>{{$val->user['first_name']}} {{$val->user['last_name']}}</td>
                                <td>{{$val->user['username']}}</td>
                                <td>{{date("Y/m/d", strtotime($val->created_at))}}</td>  
                                <td>{{date("Y/m/d h:i:A", strtotime($val->updated_at))}}</td>
                            @endforeach               
                        </tbody>                    
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
