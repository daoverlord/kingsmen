@extends('master')

@section('content')
<div class="content"> 
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h6 class="card-title font-weight-semibold">{{__('Send email')}}</h6>
                </div>
                <div class="card-body">
                    <form action="{{route('user.email.send')}}" method="post">
                    @csrf
                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">{{__('To')}}</label>
                            <div class="col-lg-10">
                                <input type="text" name="to" maxlength="200" value="{{$email}}" class="form-control readonly" required>
                            </div>
                        </div>                        
                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">{{__('Name')}}</label>
                            <div class="col-lg-10">
                                <input type="text" name="name" maxlength="200" value="{{$name}}" class="form-control readonly" required>
                            </div>
                        </div>                        
                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">{{__('Subject')}}</label>
                            <div class="col-lg-10">
                                <input type="text" name="subject" maxlength="200" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">{{__('Message')}}</label>
                            <div class="col-lg-10">
                                <textarea type="text" name="message" rows="10" class="form-control tinymce"></textarea>
                            </div>
                        </div>          
                    <div class="text-right">
                        <button type="submit" class="btn btn-success btn-sm">{{__('Send')}}</button>
                    </div>
                </form>
            </div>
        </div>    
    </div>
</div>
@stop