@extends('master')

@section('content')
    <div class="content">
        <div class="card">
            <div class="card-header header-elements-inline">
                <h6 class="card-title font-weight-semibold">{{__('Update About us')}}</h6>
            </div>
            <div class="card-body">
                <form action="{{route('about-us.update')}}" method="post">
                @csrf
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">{{__('Details')}}</label>
                        <div class="col-lg-10">
                        <textarea type="text" name="details" class="tinymce form-control">{{$value->about}}</textarea>
                        </div>
                    </div>                
                    <div class="text-right">
                    <button type="submit" class="btn btn-success btn-sm">{{__('Save')}}</button>
                    </div>
                </form>
            </div>
        </div> 
      </div>  
    </div>
@stop