@extends('master')

@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h6 class="card-title font-weight-semibold">{{__('Team')}}</h6>
                            <div class="header-elements">
                                <a class="font-weight-semibold" data-toggle="modal" data-target="#create"><i class="icon-file-plus mr-2"></i>Create review</a>
                            </div>
                    </div>
                    <div class="">
                        <table class="table datatable-responsive">
                            <thead>
                                <tr>
                                    <th>{{__('S/N')}}</th>
                                    <th>{{__('Image')}}</th>
                                    <th>{{__('Name')}}</th>
                                    <th>{{__('Occupation')}}</th>
                                    <th>{{__('Facebook')}}</th>
                                    <th>{{__('Twitter')}}</th>
                                    <th>{{__('Linkedin')}}</th>
                                    <th>{{__('Status')}}</th>
                                    <th>{{__('Created')}}</th>
                                    <th>{{__('Updated')}}</th>
                                    <th class="text-center">{{__('Action')}}</th>    
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($review as $k=>$val)
                                <tr>
                                    <td>{{++$k}}.</td>
                                    <td><img src="{{url('/')}}/asset/review/{{$val->image}}" class="blog-imaged"></td>
                                    <td>{{$val->name}}</td>
                                    <td>{{$val->position}}</td>
                                    <td>{{$val->facebook}}</td>
                                    <td>{{$val->twitter}}</td>
                                    <td>{{$val->linkedin}}</td>
                                    <td>
                                        @if($val->status==1)
                                            <span class="badge badge-success">{{__('Published')}}</span>
                                        @else
                                            <span class="badge badge-danger">{{__('Pending')}}</span>
                                        @endif
                                    </td>  
                                    <td>{{date("Y/m/d h:i:A", strtotime($val->created_at))}}</td>
                                    <td>{{date("Y/m/d h:i:A", strtotime($val->updated_at))}}</td>
                                    <td class="text-center">
                                        <div class="list-icons">
                                            <div class="dropdown">
                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    @if($val->status==1)
                                                        <a class='dropdown-item' href="{{url('/')}}/admin/unteam/{{$val->id}}"><i class="icon-eye-blocked2 mr-2"></i>{{__('Unpublish')}}</a>
                                                    @else
                                                        <a class='dropdown-item' href="{{url('/')}}/admin/pteam/{{$val->id}}"><i class="icon-eye mr-2"></i>{{__('Publish')}}</a>
                                                    @endif
                                                    <a href="{{url('/')}}/admin/team/edit/{{$val->id}}" class="dropdown-item"><i class="icon-pencil7 mr-2"></i>{{__('Edit')}}</a>
                                                    <a data-toggle="modal" data-target="#{{$val->id}}delete" class="dropdown-item"><i class="icon-bin2 mr-2"></i>{{__('Delete')}}</a>
                                                </div>
                                            </div>
                                        </div>
                                    </td>                   
                                </tr>
                                <div id="{{$val->id}}delete" class="modal fade" tabindex="-1">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">   
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>
                                            <div class="modal-body">
                                                <h6 class="font-weight-semibold">{{__('Are you sure you want to delete this?')}}</h6>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-link" data-dismiss="modal">{{__('Close')}}</button>
                                                <a  href="{{url('/')}}/admin/team/delete/{{$val->id}}" class="btn bg-danger">{{__('Proceed')}}</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach               
                            </tbody>                    
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="create" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">   
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <form action="{{url('admin/createteam')}}" method="post" enctype="multipart/form-data">
                @csrf
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">{{__('Name')}}:</label>
                            <div class="col-lg-10">
                                <input type="text" name="name" class="form-control" required>
                            </div>
                        </div> 
                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">{{__('Position')}}:</label>
                            <div class="col-lg-10">
                                <input type="text" name="position" class="form-control" required>
                            </div>
                        </div>                         
                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">{{__('Facebook')}}:</label>
                            <div class="col-lg-10">
                            <input type="url" name="facebook" class="form-control" required>
                            </div>
                        </div>                      
                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">{{__('Twitter')}}:</label>
                            <div class="col-lg-10">
                                <input type="url" name="twitter" class="form-control" required>
                            </div>
                        </div>                      
                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">{{__('Linkedin')}}:</label>
                            <div class="col-lg-10">
                                <input type="url" name="linkedin" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">{{__('Image')}}:</label>
                            <div class="col-lg-10">
                                <input type="file" name="image" class="form-input-styled" data-fouc> 
                            </div>
                        </div>                
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">{{__('Close')}}</button>
                        <button type="submit" class="btn btn-success">{{__('Save')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop