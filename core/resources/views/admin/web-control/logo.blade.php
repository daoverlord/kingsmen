@extends('master')

@section('content')
<div class="content"> 
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">{{__('Change logo')}}</h5>
                </div>
                <div class="card-body">
                    <form action="{{url('admin/updatelogo')}}" enctype="multipart/form-data" method="post">
                    @csrf
                        <div class="form-group">
                            <label>{{__('Your logo')}}:</label>
                            <input type="file" name="logo" class="form-input-styled" data-fouc>
                            @if ($errors->has('image'))
                                <div class="error">{{ $errors->first('logo') }}</div>
                            @endif
                        </div>              
                        <div class="text-right">
                        <button type="submit" class="btn btn-success btn-sm">{{__('Save')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h6 class="card-title font-weight-semibold">{{__('Logo')}}</h6>
                </div>
                <div class="card-body text-center">
                    <div class="card-img-actions d-inline-block mb-3">
                        <img class="img-fluid" src="{{url('/')}}/asset/{{$logo->image_link}}">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">{{__('Change favicon')}}</h5>
                </div>
                <div class="card-body">
                    <form action="{{url('admin/updatefavicon')}}" enctype="multipart/form-data" method="post">
                    @csrf
                        <div class="form-group">
                            <label>{{__('Your Favicon')}}:</label>
                            <input type="file" name="favicon" class="form-input-styled" data-fouc>
                        </div>              
                        <div class="text-right">
                        <button type="submit" class="btn btn-success btn-sm">{{__('Save')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h6 class="card-title font-weight-semibold">{{__('Favicon')}}</h6>
                </div>
                <div class="card-body text-center">
                    <div class="card-img-actions d-inline-block mb-3">
                        <img class="img-fluid" src="{{url('/')}}/asset/{{$logo->image_link2}}">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>  
@stop