@extends('master')

@section('content')
<div class="content"> 
    <div class="row">   
        <div class="col-md-8">
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h6 class="card-title font-weight-semibold">{{__('Edit content')}}</h6>
                </div>
                <div class="card-body">
                    <form action="{{route('homepage.update')}}" method="post">
                    @csrf
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <input type="text" name="header_title" class="form-control" value="{{$ui->header_title}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <textarea type="text" name="header_body" rows="4" class="form-control">{{$ui->header_body}}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <input type="text" name="s2_title" class="form-control" value="{{$ui->s2_title}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <textarea type="text" name="s2_body" rows="4" class="form-control">{{$ui->s2_body}}</textarea>
                            </div>
                        </div>                                                 
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <input type="text" name="s5_title" class="form-control" value="{{$ui->s5_title}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <textarea type="text" name="s5_body" rows="4" class="form-control">{{$ui->s5_body}}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <input type="text" name="s6_title" class="form-control" value="{{$ui->s6_title}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <textarea type="text" name="s6_body" rows="4" class="form-control">{{$ui->s6_body}}</textarea>
                            </div>
                        </div> 
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <input type="text" name="s7_title" class="form-control" value="{{$ui->s7_title}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <textarea type="text" name="s7_body" rows="4" class="form-control">{{$ui->s7_body}}</textarea>
                            </div>
                        </div>                         
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <input type="text" name="s8_title" class="form-control" value="{{$ui->s8_title}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <textarea type="text" name="s8_body" rows="4" class="form-control">{{$ui->s8_body}}</textarea>
                            </div>
                        </div>                        
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <textarea type="text" name="team" rows="4" class="form-control">{{$ui->team}}</textarea>
                            </div>
                        </div> 
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <input type="text" name="x1" class="form-control" value="{{$ui->x1}}">
                            </div>
                        </div>                             
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <input type="text" name="x2" class="form-control" value="{{$ui->x2}}">
                            </div>
                        </div> 
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <input type="text" name="x3" class="form-control" value="{{$ui->x3}}">
                            </div>
                        </div>                          
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <input type="text" name="x4" class="form-control" value="{{$ui->x4}}">
                            </div>
                        </div>                           
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <input type="text" name="total_assets" class="form-control" value="{{$ui->total_assets}}">
                            </div>
                        </div>                             
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <input type="text" name="experience" class="form-control" value="{{$ui->experience}}">
                            </div>
                        </div> 
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <input type="text" name="traders" class="form-control" value="{{$ui->traders}}">
                            </div>
                        </div>                          
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <input type="text" name="countries" class="form-control" value="{{$ui->countries}}">
                            </div>
                        </div>   
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <input type="text" name="item1_title" class="form-control" value="{{$ui->item1_title}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <textarea type="text" name="item1_body" rows="4" class="form-control">{{$ui->item1_body}}</textarea>
                            </div>
                        </div>                         
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <input type="text" name="item2_title" class="form-control" value="{{$ui->item2_title}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <textarea type="text" name="item2_body" rows="4" class="form-control">{{$ui->item2_body}}</textarea>
                            </div>
                        </div>                         
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <input type="text" name="item3_title" class="form-control" value="{{$ui->item3_title}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <textarea type="text" name="item3_body" rows="4" class="form-control">{{$ui->item3_body}}</textarea>
                            </div>
                        </div>     
                        <div class="text-right">
                            <button type="submit" class="btn btn-success btn-sm">{{__('Save')}}</button>
                        </div>
                </form>
            </div>
            </div>    
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body text-center">
                    <div class="card-img-actions d-inline-block mb-3">
                        <img class="blog-imaged" src="{{url('/')}}/asset/images/{{$ui->s2_image}}" alt="">
                    </div>
                    <form action="{{url('admin/section1/update')}}" enctype="multipart/form-data" method="post">
                    @csrf
                        <div class="form-group">
                            <input type="file" name="section1" class="form-input-styled" data-fouc required> 
                        </div>              
                        <div class="text-right">
                            <button type="submit" class="btn btn-success btn-sm">{{__('Save')}}</button>
                        </div>
                    </form>
                </div>
            </div>           
            <div class="card">
                <div class="card-body text-center">
                    <div class="card-img-actions d-inline-block mb-3">
                        <img class="blog-imaged" src="{{url('/')}}/asset/images/{{$ui->s3_image}}" alt="">
                    </div>
                    <form action="{{url('admin/section2/update')}}" enctype="multipart/form-data" method="post">
                    @csrf
                        <div class="form-group">
                            <input type="file" name="section2" class="form-input-styled" data-fouc required> 
                        </div>              
                        <div class="text-right">
                            <button type="submit" class="btn btn-success btn-sm">{{__('Save')}}</button>
                        </div>
                    </form>
                </div>
            </div>            
            <div class="card">
                <div class="card-body text-center">
                    <div class="card-img-actions d-inline-block mb-3">
                        <img class="blog-imaged" src="{{url('/')}}/asset/images/{{$ui->s4_image}}" alt="">
                    </div>
                    <form action="{{url('admin/section3/update')}}" enctype="multipart/form-data" method="post">
                    @csrf
                        <div class="form-group">
                            <input type="file" name="section3" class="form-input-styled" data-fouc required> 
                        </div>              
                        <div class="text-right">
                            <button type="submit" class="btn btn-success btn-sm">{{__('Save')}}</button>
                        </div>
                    </form>
                </div>
            </div>             
            <div class="card">
                <div class="card-body text-center">
                    <div class="card-img-actions d-inline-block mb-3">
                        <img class="blog-imaged" src="{{url('/')}}/asset/images/{{$ui->s7_image}}" alt="">
                    </div>
                    <form action="{{url('admin/section4/update')}}" enctype="multipart/form-data" method="post">
                    @csrf
                        <div class="form-group">
                            <input type="file" name="section4" class="form-input-styled" data-fouc required> 
                        </div>              
                        <div class="text-right">
                            <button type="submit" class="btn btn-success btn-sm">{{__('Save')}}</button>
                        </div>
                    </form>
                </div>
            </div>            
            <div class="card">
                <div class="card-body text-center">
                    <div class="card-img-actions d-inline-block mb-3">
                        <img class="blog-imaged" src="{{url('/')}}/asset/images/{{$ui->s6_image}}" alt="">
                    </div>
                    <form action="{{url('admin/section6/update')}}" enctype="multipart/form-data" method="post">
                    @csrf
                        <div class="form-group">
                            <input type="file" name="section6" class="form-input-styled" data-fouc required> 
                        </div>              
                        <div class="text-right">
                            <button type="submit" class="btn btn-success btn-sm">{{__('Save')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop