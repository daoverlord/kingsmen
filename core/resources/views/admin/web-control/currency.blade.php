@extends('master')

@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h6 class="card-title font-weight-semibold">{{__('Currency')}}</h6>
                    </div>
                    <table class="table datatable-show-all">
                        <thead>
                            <tr>
                                <th>{{__('S/N')}}</th>
                                <th>{{__('Country')}}</th>
                                <th>{{__('Name')}}</th>
                                <th>{{__('Currency')}}</th>
                                <th>{{__('Symbol')}}</th>
                                <th>{{__('Status')}}</th>
                                <th class="text-center">{{__('Action')}}</th>    
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($cur as $k=>$val)
                            <tr>
                                <td>{{++$k}}.</td>
                                <td>{{$val->country}}</td>
                                <td>{{$val->name}}</td>
                                <td>{{$val->currency}}</td>
                                <td>{{$val->symbol}}</td>
                                <td>                                    
                                    @if($val->status==1)
                                        <span class="badge badge-success">{{__('Active')}}</span>
                                    @else
                                        <span class="badge badge-danger">{{__('Pending')}}</span>
                                    @endif
                                </td>                               
                                <td class="text-center">
                                @if($val->status==0)
                                    <div class="list-icons">
                                        <div class="dropdown">
                                            <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                <i class="icon-menu9"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                            @if($val->status==0)
                                                <a class='dropdown-item' href="{{url('/')}}/admin/pcurrency/{{$val->id}}"><i class="icon-eye mr-2"></i>{{__('Set as default')}}</a>
                                            @endif
                                            </div>
                                        </div>
                                    </div>
                                    @endif  
                                </td>                 
                            </tr>
                            <div id="{{$val->id}}delete" class="modal fade" tabindex="-1">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">   
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <div class="modal-body">
                                            <h6 class="font-weight-semibold">{{__('Are you sure you want to delete this?')}}</h6>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-link" data-dismiss="modal">{{__('Close')}}</button>
                                            <a  href="{{url('/')}}/admin/branch/delete/{{$val->id}}" class="btn bg-danger">{{__('Proceed')}}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach               
                        </tbody>                    
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop