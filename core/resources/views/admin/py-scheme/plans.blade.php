@extends('master')

@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h6 class="card-title font-weight-semibold">{{__('Plans')}}</h6>
                    </div>
                    <div class="">
                        <table class="table datatable-responsive">
                            <thead>
                                <tr>
                                    <th>{{__('S/N')}}</th>
                                    <th>{{__('Name')}}</th>
                                    <th>{{__('Daily %')}}</th>                                                                       
                                    <th>{{__('Min')}}</th>
                                    <th>{{__('Max')}}</th>
                                    <th>{{__('Duration')}}</th>
                                    <th>{{__('Referral')}}</th>
                                    <th>{{__('Bonus')}}</th>
                                    <th>{{__('Status')}}</th>
                                    <th>{{__('Created')}}</th>
                                    <th>{{__('Updated')}}</th>
                                    <th class="text-center">{{__('Action')}}</th>    
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($plan as $k=>$val)
                                <tr>
                                    <td>{{++$k}}.</td>
                                    <td>{{$val->name}}</td>
                                    <td>{{$val->percent}}%</td>
                                    <td>{{$currency->symbol.number_format($val->min_deposit)}}</td>
                                    <td>{{$currency->symbol.number_format($val->amount)}}</td>
                                    <td>{{$val->duration.$val->period}}(s)</td>
                                    <td>{{$val->ref_percent}}%</td>
                                    <td>{{$val->bonus}}%</td>
                                    <td>
                                        @if($val->status==0)
                                        {{__('Disabled')}}
                                        @elseif($val->status==1)
                                        {{__('Active ')}}
                                        @endif
                                    </td>  
                                    <td>{{date("Y/m/d h:i:A", strtotime($val->created_at))}}</td>
                                    <td>{{date("Y/m/d h:i:A", strtotime($val->updated_at))}}</td>
                                    <td class="text-center">
                                        <div class="list-icons">
                                            <div class="dropdown">
                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a class='dropdown-item' href="{{url('/')}}/admin/py-plan/{{$val->id}}"><i class="icon-pencil7 mr-2"></i>{{__('Edit')}}</a>
                                                </div>
                                            </div>
                                        </div>
                                    </td>                   
                                </tr>
                                @endforeach               
                            </tbody>                    
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop