@extends('master')

@section('content')
<div class="content">
<div class="row">
  <div class="col-md-4">
    <div class="card">
      <div class="card-body">
        <div class="d-sm-flex align-item-sm-center flex-sm-nowrap">
          <div>
            <h6 class="font-weight-semibold">{{__('Users')}}</h6>
            <ul class="list list-unstyled mb-0">
              <li>{{__('Active users:')}} <span class="font-weight-semibold text-default">#{{$activeusers}}</span></li>
              <li>{{__('Blocked users:')}} <span class="font-weight-semibold text-default">#{{$blockedusers}}</span></li>
            </ul>
          </div>

          <div class="text-sm-right mb-0 mt-3 mt-sm-0 ml-auto">
            <h3 class="font-weight-semibold">#{{$totalusers}}</h3>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="card">
      <div class="card-body">
        <div class="d-sm-flex align-item-sm-center flex-sm-nowrap">
          <div>
            <h6 class="font-weight-semibold">{{__('Support Ticket')}}</h6>
            <ul class="list list-unstyled mb-0">
              <li>{{__('Open tickets:')}} <span class="font-weight-semibold text-default">
                #{{$openticket}}</span></li>
              <li>{{__('Closed tickets:')}} <span class="font-weight-semibold text-default">
                #{{$closedticket}}</span>
              </li>
            </ul>
          </div>
          <div class="text-sm-right mb-0 mt-3 mt-sm-0 ml-auto">
            <h3 class="font-weight-semibold">#{{$totalticket}}</h3>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="card">
      <div class="card-body">
        <div class="d-sm-flex align-item-sm-center flex-sm-nowrap">
          <div>
            <h6 class="font-weight-semibold">{{__('Platform Reviews')}}</h6>
            <ul class="list list-unstyled mb-0">
              <li>{{__('Published reviews:')}} <span class="font-weight-semibold text-default">
                #{{$pubreview}}</span></li>
              <li>{{__('Pending reviews:')}} <span class="font-weight-semibold text-default">
                #{{$unpubreview}}</span>
              </li>
            </ul>
          </div>
          <div class="text-sm-right mb-0 mt-3 mt-sm-0 ml-auto">
            <h3 class="font-weight-semibold">#{{$totalreview}}</h3>
          </div>
        </div>
      </div>
    </div>
  </div>  
  <div class="col-md-4">
    <div class="card">
      <div class="card-body">
        <div class="d-sm-flex align-item-sm-center flex-sm-nowrap">
          <div>
            <h6 class="font-weight-semibold">{{__('Other Deposits')}}</h6>
            <ul class="list list-unstyled mb-0">
              <li>{{__('Pending:')}} <span class="font-weight-semibold text-default">
                #{{$pendingdep}}</span></li>
              <li>{{__('Approved:')}} <span class="font-weight-semibold text-default">
                #{{$approveddep}}</span>
              </li>              
              <li>{{__('Declined:')}} <span class="font-weight-semibold text-default">
                #{{$declineddep}}</span>
              </li>
            </ul>
          </div>
          <div class="text-sm-right mb-0 mt-3 mt-sm-0 ml-auto">
            <h3 class="font-weight-semibold">#{{$totaldeposit}}</h3>
          </div>
        </div>
      </div>
    </div>
  </div>  
  <div class="col-md-4">
    <div class="card">
      <div class="card-body">
        <div class="d-sm-flex align-item-sm-center flex-sm-nowrap">
          <div>
            <h6 class="font-weight-semibold">{{__('Withdrawal')}}</h6>
            <ul class="list list-unstyled mb-0">
              <li>{{__('Pending:')}} <span class="font-weight-semibold text-default">
                #{{$pendingwd}}</span></li>
              <li>{{__('Approved:')}} <span class="font-weight-semibold text-default">
                #{{$approvedwd}}</span>
              </li>              
              <li>{{__('Declined:')}} <span class="font-weight-semibold text-default">
                #{{$declinedwd}}</span>
              </li>
            </ul>
          </div>
          <div class="text-sm-right mb-0 mt-3 mt-sm-0 ml-auto">
            <h3 class="font-weight-semibold">#{{$totalwd}}</h3>
          </div>
        </div>
      </div>
    </div>
  </div>   
  <div class="col-md-4">
    <div class="card">
      <div class="card-body">
        <div class="d-sm-flex align-item-sm-center flex-sm-nowrap">
          <div>
            <h6 class="font-weight-semibold">{{__('Investment plans')}}</h6>
            <ul class="list list-unstyled mb-0">
              <li>{{__('Active:')}} <span class="font-weight-semibold text-default">
                #{{$appplan}}</span></li>
              <li>{{__('Disabled:')}} <span class="font-weight-semibold text-default">
                #{{$penplan}}</span>
              </li>              
            </ul>
          </div>
          <div class="text-sm-right mb-0 mt-3 mt-sm-0 ml-auto">
            <h3 class="font-weight-semibold">#{{$totalplan}}</h3>
          </div>
        </div>
      </div>
    </div>
  </div>  
  <div class="col-md-4">
    <div class="card">
      <div class="card-body">
        <div class="d-sm-flex align-item-sm-center flex-sm-nowrap">
          <div>
            <h6 class="font-weight-semibold">{{__('Investment')}}</h6>
            <ul class="list list-unstyled mb-0">
              <li>{{__('Active:')}} <span class="font-weight-semibold text-default">
                #{{$appprofit}}</span></li>
              <li>{{__('Completed:')}} <span class="font-weight-semibold text-default">
                #{{$penprofit}}</span>
              </li>              
            </ul>
          </div>
          <div class="text-sm-right mb-0 mt-3 mt-sm-0 ml-auto">
            <h3 class="font-weight-semibold">#{{$totalprofit}}</h3>
          </div>
        </div>
      </div>
    </div>
  </div>
  @stop