@extends('layout')
@section('css')

@stop
@section('content')
<section id="header" class="backg backg-one bg-banner-gradient">
    <div class="container">
        <div class="backg-content-wrap">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="backg-content">
                        <span class="discount wow soneFadeUp" data-wosw-delay="0.3s">{{$set->title}}</span>
                        <h1 class="backg-title wow soneFadeUp" data-wow-delay="0.5s">{{__('About us')}}</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-shape bg-shape-bottom">
        <img src="{{url('/')}}/asset/images/shape-bg.png">
    </div>
</section>
@if(count($team)>0)
<section class="teams-single wow soneFadeUp">
    <div class="container">
        <div class="section-title text-center">
            <h3 class="sub-title">{{__('Our Team')}}</h3>
            <h2 class="title">{{__('The Experts Team')}}</h2>
            <p> {{$ui->team}}</p>
        </div>
        <div class="row">
        @foreach($team as $val)
            <div class="col-sm-4">
                <div class="team-member">
                    <div class="member-avater"><img src="{{url('/')}}/asset/review/{{$val->image}}" alt="avater">
                        <div class="layer-one">
                            <div class="team-info">
                                <span class="name">{{$val->name}}</span>
                                <p class="job">{{$val->position}}</p>
                            </div>
                        </div>

                        <ul class="member-social">
                            <li><a href="{{$val->facebook}}"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="{{$val->twitter}}"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="{{$val->linkedin}}"><i class="fab fa-linkedin-in"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        @endforeach
        </div>
    </div>
    <div class="shape-bg-left-circle">
        <div class="shape-circle-animated"></div>            
    </div>
</section>
@endif
<section class="about genera-informes wow soneFadeUp">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="">
                    <div class="section-title">
                        <p>{!!$about->about!!}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@if(count($review)>0)
<section class="testimonials-two wow soneFadeUp" id="testimonialxx">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
            <div class="section-title">
                <span class="sub-title">{{__('Reviews')}}</span>
                <h2 class="title">{{$ui->s7_title}}</h2>
                <p>{{$ui->s7_body}}</p>
            </div>
        </div>
        <div class="col-lg-8">
            <div id="testimonial-wrapper">
                <div class="swiper-container" id="testimonial-two" data-speed="700" data-autoplay="5000" data-perpage="2" data-space="50" data-breakpoints='{"991": {"slidesPerView": 1}}'>
                    <div class="swiper-wrapper">
                        @foreach($review as $vreview)
                            <div class="swiper-slide">
                                <div class="testimonial-two">
                                    <div class="testi-content-inner">
                                        <div class="testimonial-bio">
                                            <div class="avatar"><img src="{{url('/')}}/asset/review/{{$vreview->image_link}}" alt="testimonial"></div>
                                            <div class="bio-info">
                                                <h4 class="name">{{$vreview->name}}</h4>
                                                <span class="job">{{$vreview->occupation}}</span></div>
                                        </div>
                                        <div class="testimonial-content">
                                            <p>{{$vreview->review}}</p>
                                        </div>   
                                        <ul class="rating">
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                        </ul>     
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
</section>
@endif

@stop