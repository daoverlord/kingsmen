@extends('layout')
@section('css')

@stop
@section('content')
<section id="header" class="backg backg-one bg-banner-gradient">
    <div class="container">
        <div class="backg-content-wrap">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="backg-content">
                        <span class="discount wow soneFadeUp" data-wosw-delay="0.3s">{{$set->title}}</span>
                        <h1 class="backg-title wow soneFadeUp" data-wow-delay="0.5s">{{__('Frequently asked questions')}}</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-shape bg-shape-bottom">
        <img src="{{url('/')}}/asset/images/shape-bg.png">
    </div>
</section>
<section class="revolutionize revolutionize-two wow soneFadeUp">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="section-title text-left">
                    <h5 class="title">
                    {{$ui->s5_title}}
                    </h5>

                    <p>
                    {{$ui->s5_body}}
                    </p>
                </div>
            </div>
            <div id="accordion" class="col-lg-9 faq">
                @foreach($faq as $vfaq)
                <div class="card">
                    <div class="card-header" id="heading{{$vfaq->id}}">
                        <span><button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse{{$vfaq->id}}" aria-expanded="false" aria-controls="collapse{{$vfaq->id}}">{{$vfaq->question}}</button></span>
                    </div>
                    <div id="collapse{{$vfaq->id}}" class="collapse" aria-labelledby="heading{{$vfaq->id}}" data-parent="#accordion" style="">
                        <div class="card-body">
                            <p>{!! $vfaq->answer!!}</p>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>

    </div>
</section>
@stop