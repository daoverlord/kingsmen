@extends('layout')
@section('css')

@stop
@section('content')
<section id="header" class="backg backg-one bg-banner-gradient">
    <div class="container">
        <div class="backg-content-wrap">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="backg-content">
                        <span class="discount wow soneFadeUp" data-wosw-delay="0.3s">{{$set->title}}</span>
                        <h1 class="backg-title wow soneFadeUp" data-wow-delay="0.5s">
                        {{$ui->header_title}}
                        </h1>
                        <p class="description wow soneFadeUp text-dark" data-wow-delay="0.6s">
                        {{$ui->header_body}}
                        </p>
                        <a href="{{route('register')}}" class="pxs-btn backg-btn wow soneFadeUp" data-wow-delay="0.6s">{{__('Get Started')}}</a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="promo-mockup wow soneFadeLeft">
                        <img src="{{url('/')}}/asset/images/{{$ui->s6_image}}" alt="header">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-shape bg-shape-bottom">
        <img src="{{url('/')}}/asset/images/shape-bg.png">
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="feature-box">
                    <div class="row">
                        <div class="col-lg-6 flex-center">
                            <div class="featured-icon-box-wrapper style-five color-1">
                                <div class="featured-icon-box-icon"><img src="{{url('/')}}/asset/images/{{$ui->s2_image}}"></div>
                                <div class="featured-icon-box-content">
                                    <span class="featured-icon-box-title">{{$ui->item1_title}}</span>
                                    <p>{{$ui->item1_body}}</p>
                                </div>
                            </div>                            
                        </div>
                        <div class="col-lg-6 ">
                            <div class="featured-icon-box-wrapper style-five color-1">
                                <div class="featured-icon-box-icon"><img src="{{url('/')}}/asset/images/{{$ui->s3_image}}"></div>
                                <div class="featured-icon-box-content">
                                    <span class="featured-icon-box-title">{{$ui->item2_title}}</span>
                                    <p>{{$ui->item2_body}}</p>
                                </div>
                            </div>
                            <div class="featured-icon-box-wrapper style-five color-1">
                                <div class="featured-icon-box-icon"><img src="{{url('/')}}/asset/images/{{$ui->s4_image}}"></div>
                                <div class="featured-icon-box-content">
                                    <span class="featured-icon-box-title">{{$ui->item3_title}}</span>
                                    <p>{{$ui->item3_body}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 flex-center">
                <div class="section-title style-two">
                    <h2 class="title">{{$ui->s2_title}}</h2>
                    <p>{{$ui->s2_body}}</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="bg-process">
    <div class="bg-overlay-testi"></div>
    <div class="container">
        <div class="row">
        <div class="col-lg-6">
            <div class="editure-feature-image">
                <div class="image-one">
                    <img src="{{url('/')}}/asset/images/{{$ui->s7_image}}" class="wow soneFadeRight r10" data-wow-delay="0.3s" alt="feature-image">
                </div>
            </div>
        </div>
            <div class="col-lg-6">
                <div class="img-text-content">
                    <div class="section-title">
                        <h2 class="title">{{$ui->s6_title}}</h2>
                        <p>{{$ui->s6_body}}</p>
                    </div>
                    <a href="{{route('about')}}" class="sone-btn btn-light">{{__('Learn More')}}</a>
                </div>
                <!-- /.img-text-content -->
            </div>
            <!-- /.col-lg-6 -->
        </div>
        <!-- /.row -->
    </div>
</section>
<section class="services pt-150 wow soneFadeUp">
    <div class="container">
        <div class="section-title text-center">
            <span class="sub-title">{{__('Services')}}</span>
            <h2 class="title">{{__('The Most Trusted Trading Platform')}}</h2>
            <p>{{__('Here are a few reasons why you should choose us')}}</p>
        </div>

        <div class="row gap-y">
        @foreach($service as $services)
            <div class="col-md-6 col-xl-3">
                <div class="services-box-wrapper text-center">
                    <div class="my-3 services-box-icon color-1"><i class="fa fa-{{$services->icon}}"></i></div>
                    <span class="mb-20 fw-500 text-dark">{{$services->title}}</span>
                    <p class="castrooo">{{$services->details}}</p>
                </div>
            </div>
        @endforeach
        </div>
    </div>
</section>
<section class="pricing-two pt-100 wow soneFadeUp">
    <div class="container">
        <div class="section-title text-center">
            <span class="sub-title">{{__('Pricing Plan')}}</span>
            <h2 class="title">
            {{__('Choose your pricing policy which affordable')}}
            </h2>
        </div>
        <div class="row advanced-pricing-table no-gutters">
            @foreach($plan as $val)
            <div class="col-lg-3">
                <div class="pricing-table">
                    <div class="pricing-header pricing-amount">
                        <h3 class="price-title">{{$val->name}}</h3>
                        <p>{{__('Payouts wont be availabe till end of plan duration')}}</p>
                        <div class="annual_price">
                            <h2 class="price">{{$currency->symbol.$val->min_deposit}}</h2>
                        </div>
                        <div class="monthly_price">
                            <h2 class="price">{{$currency->symbol.number_format($val->min_deposit)}}</h2>
                        </div>
                        <div class="small_desc text-center">
                            <a href="javascript:void">{{__('Profit Topup is Automated')}}</a><br>
                            <a href="javascript:void">{{__('For')}} {{$val->duration}} {{$val->period}}(s)</a><br>
                            <a href="javascript:void">{{$val->percent}} {{__('Daily Percent')}}</a><br>
                            <a href="javascript:void">{{$currency->symbol.number_format($val->amount)}} {{__('Maximum Price')}}</a><br>
                            @if($val->ref_percent!=null)
                            <a href="javascript:void">{{$val->ref_percent}}% {{__('Referral Percent')}}</a><br>
                            @endif
                            @if($val->bonus!=null)
                            <a href="javascript:void">{{$val->bonus}}% {{__('Trading Bonus')}}</a><br>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
@if(count($team)>0)
<section class="teams-single wow soneFadeUp">
    <div class="container">
        <div class="section-title text-center">
            <h3 class="sub-title">{{__('Our Team')}}</h3>
            <h2 class="title">{{__('The Experts Team')}}</h2>
            <p> {{$ui->team}}</p>
        </div>
        <div class="row">
        @foreach($team as $val)
            <div class="col-sm-4">
                <div class="team-member">
                    <div class="member-avater"><img src="{{url('/')}}/asset/review/{{$val->image}}" alt="avater">
                        <div class="layer-one">
                            <div class="team-info">
                                <span class="name">{{$val->name}}</span>
                                <p class="job">{{$val->position}}</p>
                            </div>
                        </div>

                        <ul class="member-social">
                            <li><a href="{{$val->facebook}}"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="{{$val->twitter}}"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="{{$val->linkedin}}"><i class="fab fa-linkedin-in"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        @endforeach
        </div>
    </div>
    <div class="shape-bg-left-circle">
        <div class="shape-circle-animated"></div>            
    </div>
</section>
@endif
<section class="countup bg-testi">
    <div class="container">
        <div class="section-title text-center">
            <span class="sub-title">{{__('Get Assured Profits')}}</span>
            <h2 class="title">{{$ui->s8_title}}</h2>
            <p>{{$ui->s8_body}}</p>
        </div>
        <div class="countup-wrapper">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="fun-fact text-center">
                        <div class="counter">
                            <h4 class="count">{{$ui->total_assets}}</h4></div>
                        <p>{{$ui->x1}}</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="fun-fact text-center">
                        <div class="counter">
                            <h4 class="count">{{$ui->experience}}</h4></div>
                        <p>{{$ui->x2}}</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="fun-fact text-center">
                        <div class="counter">
                            <h4 class="count">{{$ui->traders}}</h4></div>
                        <p>{{$ui->x3}}</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="fun-fact text-center">
                        <div class="counter">
                            <h4 class="count">{{$ui->countries}}</h4></div>
                        <p>{{$ui->x4}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@if(count($review)>0)
<section class="testimonials-two wow soneFadeUp" id="testimonialxx">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
            <div class="section-title">
                <span class="sub-title">{{__('Reviews')}}</span>
                <h2 class="title">{{$ui->s7_title}}</h2>
                <p>{{$ui->s7_body}}</p>
            </div>
        </div>
        <div class="col-lg-8">
            <div id="testimonial-wrapper">
                <div class="swiper-container" id="testimonial-two" data-speed="700" data-autoplay="5000" data-perpage="2" data-space="50" data-breakpoints='{"991": {"slidesPerView": 1}}'>
                    <div class="swiper-wrapper">
                        @foreach($review as $vreview)
                            <div class="swiper-slide">
                                <div class="testimonial-two">
                                    <div class="testi-content-inner">
                                        <div class="testimonial-bio">
                                            <div class="avatar"><img src="{{url('/')}}/asset/review/{{$vreview->image_link}}" alt="testimonial"></div>
                                            <div class="bio-info">
                                                <h4 class="name">{{$vreview->name}}</h4>
                                                <span class="job">{{$vreview->occupation}}</span></div>
                                        </div>
                                        <div class="testimonial-content">
                                            <p>{{$vreview->review}}</p>
                                        </div>   
                                        <ul class="rating">
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                        </ul>     
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
</section>
@endif
<section class="services address-contact">
    <div class="container">
        <div class="section-title text-center">
            <h2 class="title">{{__('Don’t hesitate to contact us for')}}<br>{{__('any information.')}}</h2>
        </div>
        <div class="row gap-y">
            <div class="col-md-4">
                <div class="services-box-wrapper text-center">
                    <div class="my-3 services-box-icon color-1"><i class="fas fa-map-marker-alt xcolls"></i></div>
                    <span class="mb-20 fw-500 text-dark">{{__('Our Location')}}</span>
                    <p class="castrooo">{{$set->address}}</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="services-box-wrapper text-center">
                    <div class="my-3 services-box-icon color-1"><i class="fas fa-border-all xcolls"></i></div>
                    <span class="mb-20 fw-500 text-dark">{{__('Get In Touch')}}</span>
                    <p class="castrooo">{{__('Also find us social media below')}}</p>
                    <ul class="social-link">
                        @foreach($social as $socials)
                            @if(!empty($socials->value))
                        <li><a href="{{$socials->value}}" class="icon-{{$socials->type}}"><i class="fab fa-{{$socials->type}}"></i></a></li>
                            @endif
                        @endforeach 
                    </ul>
                </div>
            </div>
            <div class="col-md-4">
              <div class="services-box-wrapper text-center">
                <div class="my-3 services-box-icon color-1"><i class="fas fa-envelope-open xcolls"></i></div>
                <span class="mb-20 fw-500 text-dark">{{__('Email & Phone')}}</span>
                <p class="castrooo">{{$set->email}}<br>{{$set->mobile}}</p>
              </div>
            </div>
        </div>
    </div>
</section>
@stop