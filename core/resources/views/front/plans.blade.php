@extends('layout')
@section('css')

@stop
@section('content')
<section id="header" class="backg backg-one bg-banner-gradient">
    <div class="container">
        <div class="backg-content-wrap">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="backg-content">
                        <span class="discount wow soneFadeUp" data-wosw-delay="0.3s">{{$set->title}}</span>
                        <h1 class="backg-title wow soneFadeUp" data-wow-delay="0.5s">
                        {{__('Plans that gives assured profits')}}
                        </h1><br>
                        <a href="{{route('register')}}" class="pxs-btn backg-btn wow soneFadeUp" data-wow-delay="0.6s">{{__('Get Started')}}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-shape bg-shape-bottom">
        <img src="{{url('/')}}/asset/images/shape-bg.png">
    </div>
</section>
<section class="pricing-two pt-100 wow soneFadeUp">
    <div class="container">
        <div class="section-title text-center">
            <span class="sub-title">{{__('Pricing Plan')}}</span>
            <h2 class="title">
            {{__('Choose your pricing policy which affordable')}}
            </h2>
        </div>
        <div class="row advanced-pricing-table no-gutters">
            @foreach($plan as $val)
            <div class="col-lg-3">
                <div class="pricing-table">
                    <div class="pricing-header pricing-amount">
                        <h3 class="price-title">{{$val->name}}</h3>
                        <p>{{__('Payouts wont be availabe till end of plan duration')}} </p>
                        <div class="annual_price">
                            <h2 class="price">{{$currency->symbol.$val->min_deposit}}</h2>
                        </div>
                        <div class="monthly_price">
                            <h2 class="price">{{$currency->symbol.number_format($val->min_deposit)}}</h2>
                        </div>
                        <div class="small_desc text-center">
                            <a href="javascript:void">{{__('Profit Topup is Automated')}}</a><br>
                            <a href="javascript:void">{{__('For')}} {{$val->duration}} {{$val->period}}(s)</a><br>
                            <a href="javascript:void">{{$val->percent}} {{__('Daily Percent')}}</a><br>
                            <a href="javascript:void">{{$currency->symbol.number_format($val->amount)}} {{__('Maximum Price')}}</a><br>
                            @if($val->ref_percent!=null)
                                <p class="text-xs text-dark mb-0">{{$val->ref_percent}}% {{__('Referral Percent')}}</p>
                            @endif                  
                            @if($val->bonus!=null)
                                <p class="text-xs text-dark mb-0">{{$val->bonus}}% {{__('Trading Bonus')}}</p>
                            @endif
                            <br>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
@stop