@extends('layout')
@section('css')

@stop
@section('content')
<section id="header" class="backg backg-one bg-banner-gradient">
    <div class="container">
        <div class="backg-content-wrap">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="backg-content">
                        <span class="discount wow soneFadeUp" data-wosw-delay="0.3s">{{$set->title}}</span>
                        <h1 class="backg-title wow soneFadeUp" data-wow-delay="0.5s">{{__('Contact us')}}</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-shape bg-shape-bottom">
        <img src="{{url('/')}}/asset/images/shape-bg.png">
    </div>
</section>
<section id="contact" class="wow soneFadeUp" data-wow-delay="0.3s">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="contact-froms">
                    <form action="{{route('contact-submit')}}" method="post" class="contact-form" data-saasone="contact-froms">
                    @csrf
                        <div class="row">
                            <div class="col-md-12">
                            <input type="text" name="name" placeholder="Name" required>
                            <input type="text" name="mobile" placeholder="Mobile" required>
                            </div>
                        </div>
                        <input type="email" name="email" placeholder="Email" required>
                        <textarea name="message" placeholder="Your Message" required></textarea> 

                        <button type="submit" class="sone-btn">{{__('Send')}}</button>
                        
                        <div class="form-result alert">
                            <div class="content"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="services address-contact">
    <div class="container">
        <div class="section-title text-center">
            <h2 class="title">{{__('Don’t hesitate to contact us for')}}<br>{{__('any information')}}</h2>
        </div>
        <div class="row gap-y">
            <div class="col-md-4">
                <div class="services-box-wrapper text-center">
                    <div class="my-3 services-box-icon color-1"><i class="fas fa-map-marker-alt xcolls"></i></div>
                    <span class="mb-20 fw-500 text-dark">{{__('Our Location')}}</span>
                    <p class="castrooo">{{$set->address}}</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="services-box-wrapper text-center">
                    <div class="my-3 services-box-icon color-1"><i class="fas fa-border-all xcolls"></i></div>
                    <span class="mb-20 fw-500 text-dark">{{__('Get In Touch')}}</span>
                    <p class="castrooo">{{__('Also find us social media below')}}</p>
                    <ul class="social-link">
                        @foreach($social as $socials)
                            @if(!empty($socials->value))
                        <li><a href="{{$socials->value}}" class="icon-{{$socials->type}}"><i class="fab fa-{{$socials->type}}"></i></a></li>
                            @endif
                        @endforeach 
                    </ul>
                </div>
            </div>
            <div class="col-md-4">
              <div class="services-box-wrapper text-center">
                <div class="my-3 services-box-icon color-1"><i class="fas fa-envelope-open xcolls"></i></div>
                <span class="mb-20 fw-500 text-dark">{{__('Email & Phone')}}</span>
                <p class="castrooo">{{$set->email}}<br>{{$set->mobile}}</p>
              </div>
            </div>
        </div>
    </div>
</section>
@stop