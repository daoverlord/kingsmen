@extends('userlayout')

@section('content')
<div class="container-fluid mt--6">
  <div class="content-wrapper">
    <div class="row" id="earnings">
      <div class="col-lg-8">
        @foreach($profit as $k=>$val)
            <div class="card bg-future">
              <!-- Card body -->
              <div class="card-body">
                <div class="row align-items-center">
                  <div class="col">
                    <h4 class="mb-0 text-dark">#{{$val->trx}}</h4>
                  </div>
                </div>
                <div class="row align-items-center">
                  <div class="col">
                    <p class="text-xs text-dark mb-0">{{__('Plan:')}} {{$val->plan->name}}</p>
                    <p class="text-xs text-dark mb-0">{{__('Started:')}} {{date("Y/m/d h:i:A", strtotime($val->date))}}</p>
                    <p class="text-xs text-dark mb-0">{{__('Deposit:')}} {{$currency->symbol.number_format($val->amount)}}</p>
                    <p class="text-xs text-dark mb-0">{{__('Percent:')}} {{$val->plan->percent}}%</p>
                    <p class="text-xs text-dark mb-0">{{__('Duration:')}} {{$val->plan->duration.' '.$val->plan->period}}(s)</p>
                    <ul class="list-group list-group-flush list my--1 mb-3">
                      <li class="list-group-item px-0">
                        <div class="row align-items-center">
                          <div class="col">
                            <span class="text-xs text-dark mb-0">{{$currency->symbol.number_format($val->profit)}} / {{$currency->symbol.number_format($val->amount)}} @if($val->plan->bonus!=null)+ {{__('Trading Bonus')}} {{$currency->symbol.number_format($val->amount*$val->plan->bonus/100)}} @endif</span>
                            <div class="progress progress-xs mb-0">
                              <div class="progress-bar bg-progress" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: {{($val->profit*100)/$val->amount}}%;"></div>
                            </div>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
        @endforeach
      </div>
      <div class="col-lg-4">
        <div class="card bg-future">
          <div class="card-body">
            <p class="card-text text-xs text-dark">{{__('Payouts wont be availabe till end of plan duration. Interest means profit and compound is sum of money invested plus profit. Trading bonus is a certain percent of your compound interest. If interest reads minus, dont invest, you will lose money')}}</p>
          </div>
        </div>
        @foreach($plan as $val)
          <div class="">
            <div class="pricing card-group flex-column flex-md-row mb-3">
              <div class="card card-pricing border-0 bg-future text-center mb-2">
                <div class="card-header bg-transparent">
                  <div class="row align-items-center">
                    <div class="col-lg-12 text-right">
                      <a href="#" data-toggle="modal" data-target="#calculate{{$val->id}}" class="btn btn-sm btn-neutral">{{__('Calculate')}}</a>
                      <div class="modal fade" id="calculate{{$val->id}}" tabindex="-1" role="dialog" aria-labelledby="modal-form" aria-hidden="true">
                        <div class="modal-dialog modal- modal-dialog-centered modal-sm" role="document">
                          <div class="modal-content">
                            <div class="modal-body p-0">
                              <div class="card bg-progress border-0 mb-0">
                                <div class="card-header bg-transparent pb-5">
                                  <div class="text-white text-center mt-2 mb-3"><small>{{__('Calculate profit')}}</small></div>
                                  <div class="btn-wrapper text-center">
                                    <h4 class="text-uppercase ls-1 text-white py-3 mb-0">{{$val->name}}</h4>
                                  </div>
                                </div>
                                <div class="card-body">
                                  <form role="form" action="{{url('user/calculate')}}" method="post">
                                  @csrf
                                    <div class="form-group mb-3">
                                      <div class="input-group input-group-alternative">
                                        <div class="input-group-prepend">
                                          <span class="input-group-text">{{$currency->symbol}}</span>
                                        </div>
                                        <input type="number" step="any" class="form-control" placeholder="" name="amount" required>
                                        <input type="hidden" name="plan_id" value="{{$val->id}}"> 
                                      </div>
                                    </div>
                                    <div class="text-center">
                                      <button type="submit" class="btn btn-success btn-sm my-4">{{__('Calculate')}}</button>
                                    </div>
                                  </form>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div> 
                    </div>
                      <div class="modal fade" id="buy{{$val->id}}" tabindex="-1" role="dialog" aria-labelledby="modal-form" aria-hidden="true">
                        <div class="modal-dialog modal- modal-dialog-centered modal-sm" role="document">
                          <div class="modal-content">
                            <div class="modal-body p-0">
                              <div class="card bg-progress border-0 mb-0">
                                <div class="card-header bg-transparent pb-5">
                                  <div class="text-white text-center mt-2 mb-3"><small>{{__('Purchase plan')}}</small></div>
                                  <div class="btn-wrapper text-center">
                                    <h4 class="text-uppercase ls-1 text-white py-3 mb-0">{{$val->name}}</h4>
                                  </div>
                                </div>
                                <div class="card-body">
                                  <form role="form" action="{{url('user/buy')}}" method="post">
                                  @csrf
                                    <div class="form-group mb-3">
                                      <div class="input-group input-group-merge input-group-alternative">
                                        <div class="input-group-prepend">
                                          <span class="input-group-text">{{$currency->symbol}}</span>
                                        </div>
                                        <input type="number" step="any" class="form-control" placeholder="" name="amount" required>
                                        <input type="hidden" name="plan" value="{{$val->id}}">
                                      </div>
                                    </div>
                                    <div class="text-center">
                                      <button type="submit" class="btn btn-success btn-sm my-4">{{__('Purchase')}}</button>
                                    </div>
                                  </form>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                  </div>
                </div>
                <div class="card-body px-lg-7">
                  <h4 class="card-title mb-0">{{$val->name}}</h4>
                  <div class="text-xl text-future">{{$currency->symbol.$val->min_deposit}} - {{$currency->symbol.$val->amount}}</div>
                  <p class="card-text text-sm text-dark text-uppercase">{{__('For')}}  {{$val->duration.' '.$val->period}}(s)</p>
                  <p class="text-xs text-dark mb-0">{{$val->percent}}% {{__('Daily Top Up')}}</p>
                  <p class="text-xs text-dark mb-0">{{__('Maximum Price')}} - {{$currency->symbol.number_format($val->amount)}} </p>
                  <p class="text-xs text-dark mb-0">{{__('Interest')}} {{($val->percent*castrotime($val->duration.' '.$val->period))-100}}%</p>
                  <p class="text-xs text-dark mb-0">{{__('Compound Interest')}}  {{$val->percent*castrotime($val->duration.' '.$val->period)}}%</p>
                  @if($val->ref_percent!=null)
                    <p class="text-xs text-dark mb-0">{{$val->ref_percent}}% {{__('Referral Percent')}}</p>
                  @endif                  
                  @if($val->bonus!=null)
                    <p class="text-xs text-dark mb-0">{{$val->bonus}}% {{__('Trading Bonus')}}</p>
                  @endif
                  <br>
                  <a href="#" data-toggle="modal" data-target="#buy{{$val->id}}"  class="btn btn-sm btn-success">{{__('Purchase plan')}}</a>
                </div>
              </div>
            </div>
          </div>
        @endforeach
      </div>
    </div>
@stop