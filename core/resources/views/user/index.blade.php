@extends('userlayout')

@section('content')
<div class="container-fluid mt--6">
  <div class="content-wrapper">
  <div class="row">
    <div class="col-lg-8">
      <div class="row">
          <div class="col-lg-6">
            <div class="card bg-future">
              <!-- Card header -->
              <div class="card-header">
                <div class="row align-items-center">
                  <div class="col-8">
                    <!-- Surtitle -->
                    <h5 class="surtitle text-future">{{__('Last 5 trades')}}</h5>
                    <!-- Title -->
                    <h5 class="h3 mb-0 text-dark">{{__('Progress track')}}</h5>
                  </div>
                </div>
              </div>
              <!-- Card body -->
              <div class="card-body">
                <!-- List group -->
                <ul class="list-group list-group-flush list my--3">
                  @foreach($profit as $k=>$val)
                  <li class="list-group-item px-0">
                    <div class="row align-items-center">
                      <div class="col">
                        <h5 class="text-dark">#{{$val->trx}} @ {{$val->plan->name}} [{{number_format($val->profit)}}/{{number_format($val->amount).$currency->name}}]</h5>
                        <div class="progress progress-xs mb-0">
                          <div class="progress-bar bg-progress" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: {{($val->profit*100)/$val->amount}}%;"></div>
                        </div>
                      </div>
                    </div>
                  </li>
                  @endforeach
                </ul>
              </div>
            </div>
            <div class="card bg-future border-0">
              <div class="card-body">
                <div class="row">
                  <div class="col">
                    <h4 class="card-title mb-0">{{__('Available Profit')}}</h4>
                    <span class="mb-0 text-future">{{$currency->symbol.number_format($user->profit)}}</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="card bg-future border-0">
              <div class="card-body">
                <div class="row">
                  <div class="col">
                    <h4 class="card-title mb-0">{{__('Referral Earnings')}}</h4>
                    <span class="mb-0 text-future">{{$currency->symbol.number_format($user->ref_bonus)}}</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="card bg-future">
              <div class="card-body">
                <div class="row align-items-center">
                  <div class="col text-center">
                    <h4 class="mb-4 text-future">
                    {{__('Statistics')}}
                    </h4>
                    <span class="text-sm text-dark mb-0"><i class="fa fa-google-wallet"></i>{{__(' Received')}}</span><br>
                    <span class="text-xl text-dark mb-0">{{$currency->name}} {{number_format($received)}}.00</span><br>
                    <hr>
                  </div>
                </div>
                <div class="row align-items-center">
                  <div class="col">
                    <div class="my-4">
                      <span class="surtitle">{{__('Pending')}}</span><br>
                      <span class="surtitle ">{{__('Total')}}</span>
                    </div>
                  </div>
                  <div class="col-auto">
                    <div class="my-4">
                      <span class="surtitle ">{{$currency->name}} {{number_format($pending)}}.00</span><br>
                      <span class="surtitle text-future">{{$currency->name}} {{number_format($total)}}.00</span>
                    </div>
                  </div>
                </div>

              </div>
            </div>
            <div class="pricing card-group flex-column flex-md-row mb-3">
              <div class="card card-pricing border-0 bg-future text-center mb-4">
                <a href="#" data-toggle="modal" data-target="#buy{{$plan->id}}">
                  <div class="card-body px-lg-7">
                    <p class="card-text text-xs text-dark">{{__('Payouts wont be availabe till end of plan duration. Interest means profit and compound is sum of money invested plus profit. Trading bonus is a certain percent of your compound interest.')}}</p>
                    <h4 class="card-title mb-0">{{__('Most Purchased')}}</h4>
                    <div class="display-2 text-future">{{$currency->symbol.number_format($plan->min_deposit)}}</div>
                    <p class="card-text text-sm text-dark text-uppercase">{{__('For')}}  {{$plan->duration.' '.$plan->period}}(s)</p>
                    <p class="text-xs text-dark mb-0">{{$plan->percent}}% {{__('Daily Top Up')}}</p>
                    <p class="text-xs text-dark mb-0">{{__('Maximum Price')}} - {{$currency->symbol.number_format($plan->amount)}} </p>
                    <p class="text-xs text-dark mb-0">{{__('Interest')}} {{($plan->percent*castrotime($plan->duration.' '.$plan->period))-100}}%</p>
                    <p class="text-xs text-dark mb-0">{{__('Compound Interest')}}  {{$plan->percent*castrotime($plan->duration.' '.$plan->period)}}%</p>
                    @if($plan->ref_percent!=null)
                      <p class="text-xs text-dark mb-0">{{$plan->ref_percent}}% {{__('Referral Percent')}}</p>
                    @endif                  
                    @if($plan->bonus!=null)
                      <p class="text-xs text-dark mb-0">{{$plan->bonus}}% {{__('Trading Bonus')}}</p>
                    @endif
                  </div>
                </a>
              </div>
              <div class="modal fade" id="buy{{$plan->id}}" tabindex="-1" role="dialog" aria-labelledby="modal-form" aria-hidden="true">
                <div class="modal-dialog modal- modal-dialog-centered modal-sm" role="document">
                  <div class="modal-content">
                    <div class="modal-body p-0">
                      <div class="card bg-progress border-0 mb-0">
                        <div class="card-header bg-transparent pb-5">
                          <div class="text-white text-center mt-2 mb-3"><small>{{__('Purchase plan')}}</small></div>
                          <div class="btn-wrapper text-center">
                              <h4 class="text-uppercase ls-1 text-white py-3 mb-0">{{$plan->name}}</h4>
                          </div>
                        </div>
                        <div class="card-body">
                          <form role="form" action="{{url('user/buy')}}" method="post">
                          @csrf
                            <div class="form-group mb-3">
                              <div class="input-group input-group-merge input-group-alternative">
                                <div class="input-group-prepend">
                                  <span class="input-group-text">{{$currency->symbol}}</span>
                                </div>
                                <input type="number" step="any" class="form-control" placeholder="" name="amount" required>
                                <input type="hidden" name="plan" value="{{$plan->id}}">
                              </div>
                            </div>
                            <div class="text-center">
                              <button type="submit" class="btn btn-success btn-sm my-4">{{__('Purchase')}}</button>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
    </div>
    <div class="col-lg-4">
      <div class="card bg-future border-0">
        <div class="card-body">
          <div class="row">
            <div class="col">
              <h3 class="card-title mb-1">{{__('Two Factor Authentication')}}</h3>
              <span class="badge badge-pill badge-future">
                @if($user->fa_status==0)
                {{__('Disabled')}}
                @else
                {{__('Active')}}
                @endif
              </span>
            </div>
          </div>
        </div>
      </div>
      @if($set->kyc)
        <div class="card bg-future" id="kyc">
          <div class="card-body">
            <h3 class="card-title mb-3">{{__('Identity verification')}}</h3>
            <p class="card-text text-xs text-dark">{{__('Please Upload an Identity Document, for example, Driver Licence, Voters Card, International Passport, National ID.')}}</p>
            <span class="badge badge-pill badge-future mb-3">
              @if($user->kyc_status==0)
              {{__('Under Review')}}
              @else
              {{__('Verified')}}
              @endif
            </span>
            @if(empty($user->kyc_link))
                <form method="post" action="{{url('user/kyc')}}" enctype="multipart/form-data">
                @csrf
                    <div class="form-group">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" id="customFileLang1" name="image" lang="en">
                        <label class="custom-file-label" for="customFileLang1">{{__('Select document')}}</label>
                      </div>
                    </div>
                  <div class="text-right">
                    <input type="submit" class="btn btn-success btn-sm" value="Upload">
                  </div>
                </form>
            @endif
          </div>
        </div>
      @endif
      @if($set->referral==1)
      <div class="card bg-future">
        <div class="card-body">
          <h3 class="card-title mb-3">{{__('Referral link')}}</h3>
          <p class="card-text text-xs text-dark">{{__('Automatically Top up your Balance by Sharing your Referral Link, Earn a percentage of whatever Plan your Referred user Buys.')}}</p>
          <span class="form-text text-xs">{{url('/')}}/referral/{{$user->username}}</span>
          <button type="button" class="btn-icon-clipboard" data-clipboard-text="{{url('/')}}/referral/{{$user->username}}" title="Copy">{{__('Copy')}}</button>
        </div>
      </div>
      @endif
  </div>  
</div>
@stop