@extends('userlayout')

@section('content')
<div class="container-fluid mt--6">
  <div class="content-wrapper">
    <div class="row">
      <div class="col-lg-12">
        <div class="card bg-future">
          <div class="card-body text-dark">
            <div class="">
              <h3 class="text-dark">{{$gate->gateway['name']}}</h3>
              <span class="mt-0 mb-5 text-xs">{{__('Amount')}}:{{$currency->symbol.number_format($gate->amount)}}</span><br>
              <span class="mt-0 mb-5 text-xs">{{__('Charge')}}:{{$currency->symbol.$gate->charge}}</span><br>
              <span class="mt-0 mb-5 text-xs">{{__('Total')}}:{{$currency->symbol.number_format($gate->amount+$gate->charge)}}</span><br><br>
                <form action="{{route('deposit.confirm')}}" method="post">
                  @csrf
                  <button type="submit" class="btn btn-success btn-sm">{{__('Confirm')}}</button>
                </form>
            </div>
          </div>
        </div>
      </div>
    </div>
@stop