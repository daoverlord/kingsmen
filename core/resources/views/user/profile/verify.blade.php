@extends('userlayout')
@section('content')
<div class="container-fluid mt--6">
  <div class="content-wrapper">
    <h4 class="display-2 text-default">
      @if(Auth::user()->status == 1 )
      {{__('Account has been blocked')}}
      @else
      {{__('Account verification')}}
      @endif
    </h4>
    @if(Auth::user()->email_verify == 0 )
      <div class="card">
        <div class="card-header header-elements-inline">
          <div class="row justify-content-between align-items-center">
              <div class="col">
                <h3 class="mb-0">{{__('Verify Email address')}}</h3>
              </div>
              <div class="col-auto">
                <form action="{{route('user.send-emailVcode') }}" method="post">
                  @csrf
                  <input type="hidden" name="id" value="{{Auth::user()->id}}">
                  <input type="hidden" class="form-control" value="{{Auth::user()->email}}" readonly required>              
                  <button type="submit" class="btn btn-neutral btn-sm"><i class="fa fa-spinner"></i>{{__(' Resend code')}}</button>
                </form>
              </div>
          </div>
        </div>      
        <div class="card-body">
          <form action="{{ route('user.email-verify')}}" method="post">
            @csrf
            <div class="form-group row">
              <label class="col-form-label col-lg-12">{{__('Enter Code')}}</label>
              <div class="col-lg-6">
                <input type="hidden"  name="id" value="{{Auth::user()->id}}">
                <input type="text" name="email_code" class="form-control" placeholder="Verification Code" required>
              </div>
            </div>               
            <div class="text-left">
              <button type="submit" class="btn btn-success btn-sm">{{__('Verify Code')}}</button>
            </div>
          </form>
        </div>
      </div>    
      @elseif(Auth::user()->phone_verify == 0 )
      <div class="card">
        <div class="card-header header-elements-inline">
          <div class="row justify-content-between align-items-center">
              <div class="col">
                <h3 class="mb-0">{{__('Verify Mobile number')}}</h3>
              </div>
              <div class="col-auto">
                <form action="{{route('user.sms-verify') }}" method="post">
                  @csrf
                  <input type="hidden" name="id" value="{{Auth::user()->id}}">
                  <input type="hidden" class="form-control" value="{{Auth::user()->phone}}" readonly required>              
                  <button type="submit" class="btn btn-neutral btn-sm"><i class="fa fa-spinner"></i>{{__(' Resend code')}}</button>
                </form>
              </div>
          </div>
        </div>      
        <div class="card-body">
          <form action="{{ route('user.sms-verify')}}" method="post">
            @csrf
            <div class="form-group row">
              <label class="col-form-label col-lg-12">{{__('Enter Code')}}</label>
              <div class="col-lg-6">
                <input type="hidden"  name="id" value="{{Auth::user()->id}}">
                <input type="text" name="sms_code" class="form-control" placeholder="Verification Code" required>
              </div>
            </div>               
            <div class="text-left">
              <button type="submit" class="btn btn-success btn-sm">{{__('Verify Code')}}</button>
            </div>
          </form>
        </div>
      </div>
    @endif
@stop