@extends('userlayout')

@section('content')
<div class="container-fluid mt--6">
  <div class="content-wrapper">
    <div class="row">  
      <div class="col-lg-8">
        <div class="row">
          @foreach($earning as $k=>$val)
            <div class="col-md-12">
                <div class="card bg-future">
                  <!-- Card body -->
                  <div class="card-body">
                    <div class="row align-items-center">
                      <div class="col-12">
                        <!-- Title -->
                        <h5 class="h4 mb-0">#{{$val->ref_id}}</h5>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col">
                        <p class="text-xs text-dark mb-0">{{__('Amount:')}} {{$currency->symbol.number_format($val->amount)}}</p>
                        <p class="text-xs text-dark mb-0">{{__('From:')}} {{$val->user['first_name']}} {{$val->user['last_name']}}</p>
                        <p class="text-xs text-dark mb-0">{{__('Plan:')}} {{$val->plan['name']}}</p>
                        <p class="text-xs text-dark mb-0">{{__('Created:')}} {{date("Y/m/d h:i:A", strtotime($val->created_at))}}</p>
                        <p class="text-xs text-dark mb-0">{{__('Updated:')}} {{date("Y/m/d h:i:A", strtotime($val->updated_at))}}</p>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
          @endforeach
        </div>
      </div>
      <div class="col-lg-4">
        <div class="card">
          <div class="card-header">
            <h5 class="h3 mb-0">{{__('Referrals')}}</h5>
          </div>
          <div class="card-body">
            <ul class="list-group list-group-flush list my--3">
              @foreach($referral as $k=>$val)
              <li class="list-group-item px-0">
                <div class="row align-items-center">
                  <div class="col">
                    <h4 class="mb-0">
                      <a href="javascript:void;">{{$val->user['first_name']}} {{$val->user['last_name']}}</a>
                    </h4>
                    <small>{{$val->user['username']}} @ {{date("Y/m/d h:i:A", strtotime($val->created_at))}}</small>
                  </div>
                </div>
              </li>
              @endforeach
            </ul>
          </div>
        </div>
      </div>
      </div> 
    </div>
@stop